# DDBMS Project

![logo](markdown/ddbms.png)

This is my project on Distributed Database Management System (advance database) course at Pasargad university. I did this project individually in about two months with vs/blend and C# programming language and I used WPF technology for GUI.  Also this project was selected as the best project by the course instructor.
This is included 3 projects:
## Project 1:
- Query Executer
- Table Manager

## Project 2:
a lock manager simulator included:
- Transaction Moderator
- Auto/Manual Transaction Generator
- Manual Commit/Abort
- Simulation (Auto Commit/Abort)
- Deadlock Handling

## Project 3:
- Create customized dataset
- Hash function included 6 algorithm
- hash table generator

## Requirement:
- [x] SQL Server
- [x] .NET Framework

## Quick Review:

![Download](markdown/video.mp4)