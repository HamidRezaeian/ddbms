﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.View_Model.UI_Management
{
    public static class UIM_Main
    {
        public static Project3.View.Main_View.Main main { get; set; }
        private static bool _IsFinished_Dataset = false;
        private static bool _IsFinished_HashFunction = false;
        private static bool _IsFinished_Hashjoin = true;
        public static bool IsFinished_Dataset
        {
            get { return _IsFinished_Dataset; }
            set
            {
                _IsFinished_Dataset = value;
                SelectTab("Datasets");
            }
        }
        public static bool IsFinished_HashFunction
        {
            get { return _IsFinished_HashFunction; }
            set
            {
                _IsFinished_HashFunction = value;
                SelectTab("HashFunction");
            }
        }
        public static bool IsFinished_Hashjoin
        {
            get { return _IsFinished_Hashjoin; }
            set
            {
                _IsFinished_Hashjoin = value;
                SelectTab("Hashjoin");
            }
        }
        public static void SelectTab(string Tab_name)
        {
                switch (Tab_name)
                {
                case "Datasets":
                    if (main.Toolbar.Button_Dataset.IsChecked.Value)
                    {
                        main.Datasets.Visibility = System.Windows.Visibility.Visible;
                        main.HashFunction.Visibility = System.Windows.Visibility.Collapsed;
                        main.HashJoin.Visibility = System.Windows.Visibility.Collapsed;
                        _IsFinished(IsFinished_Dataset, "Datasets");
                    }
                    break;
                case "HashFunction":
                    if (main.Toolbar.Button_HashFunction.IsChecked.Value)
                    {
                        main.Datasets.Visibility = System.Windows.Visibility.Collapsed;
                        main.HashFunction.Visibility = System.Windows.Visibility.Visible;
                        main.HashJoin.Visibility = System.Windows.Visibility.Collapsed;
                        _IsFinished(IsFinished_HashFunction, "HashFunction");
                    }
                    break;
                case "Hashjoin":
                    if (main.Toolbar.Button_HashJoin.IsChecked.Value)
                    {
                        main.Datasets.Visibility = System.Windows.Visibility.Collapsed;
                        main.HashFunction.Visibility = System.Windows.Visibility.Collapsed;
                        main.HashJoin.Visibility = System.Windows.Visibility.Visible;
                        _IsFinished(IsFinished_Hashjoin, "Hashjoin");
                    }
                    break;
                default:
                    break;
                }
         }
        
        private static void _IsFinished(bool isFinished, string Tab_name)
        {
            switch (Tab_name)
            {
                case "Datasets":
                    if (isFinished)
                    {
                        main.Toolbar.Button_Dataset.IsEnabled = true;
                        main.Toolbar.Button_HashFunction.IsEnabled = true;
                        //main.Toolbar.Button_HashJoin.IsEnabled = false;
                    }
                    else
                    {
                        main.Toolbar.Button_Dataset.IsEnabled = true;
                        main.Toolbar.Button_HashFunction.IsEnabled = false;
                        main.Toolbar.Button_HashJoin.IsEnabled = false;
                    }
                    break;
                case "HashFunction":
                    if (isFinished)
                    {
                        main.Toolbar.Button_HashFunction.IsEnabled = true;
                        main.Toolbar.Button_Dataset.IsEnabled = true;
                        main.Toolbar.Button_HashJoin.IsEnabled = true;
                    }
                    else
                    {
                        main.Toolbar.Button_HashFunction.IsEnabled = true;
                        main.Toolbar.Button_Dataset.IsEnabled = true;
                        main.Toolbar.Button_HashJoin.IsEnabled = false;
                    }
                    break;
                case "Hashjoin":
                    if (isFinished)
                    {
                        main.Toolbar.Button_HashJoin.IsEnabled = true;
                        main.Toolbar.Button_Dataset.IsEnabled = true;
                        main.Toolbar.Button_HashFunction.IsEnabled = true;
                    }
                    else
                    {
                        main.Toolbar.Button_HashJoin.IsEnabled = true;
                        main.Toolbar.Button_Dataset.IsEnabled = false;
                        main.Toolbar.Button_HashFunction.IsEnabled = false;
                    }
                    break;
                default:
                    break;
            }
        }

    }
}
