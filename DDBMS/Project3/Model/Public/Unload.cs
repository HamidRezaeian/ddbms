﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model.Public
{
    class Unload
    {
        public void unload()
        {


            if (Project3.Model.Dataset.StaticData.StaticData.Datasets[0] != null) Project3.Model.Dataset.StaticData.StaticData.Datasets[0].Clear();
            if (Project3.Model.Dataset.StaticData.StaticData.Datasets[1] != null) Project3.Model.Dataset.StaticData.StaticData.Datasets[1].Clear();

            if (Project3.Model.Dataset.StaticData.StaticData.DatasetsColumnModel[0] != null)
                Project3.Model.Dataset.StaticData.StaticData.DatasetsColumnModel[0].Clear();
            if (Project3.Model.Dataset.StaticData.StaticData.DatasetsColumnModel[1] != null)
                Project3.Model.Dataset.StaticData.StaticData.DatasetsColumnModel[1].Clear();

            Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1.Clear();
            Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD2.Clear();

            Project3.Model.Dataset.StaticData.StaticData.KeyFound = false;
            Project3.Model.Dataset.StaticData.StaticData.KeyIsNull = false;

            Project3.Model.Dataset.StaticData.StaticData.KeyIndex = null;
            //Project3.Model.Dataset.StaticData.StaticData.KeyIndex[0] = new Dataset.Keys.Model.KeyModel();
            //Project3.Model.Dataset.StaticData.StaticData.KeyIndex[1] = new Dataset.Keys.Model.KeyModel();
            Project3.Model.Dataset.StaticData.StaticData.NumericConfig = "TenPart";
            Project3.Model.Dataset.StaticData.StaticData.StringConfig= "Alphabet2";
            Project3.Model.Dataset.StaticData.StaticData.Result.Clear();

        }
    }
}
