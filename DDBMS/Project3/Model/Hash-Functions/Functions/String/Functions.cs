﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model.Hash_Functions.Functions.String
{
    class Functions
    {
        public int Alphabet(string key)
        {
            char c =Convert.ToChar(key.Substring(0, 1));
            int ascii = Convert.ToInt32(c);
            //return ascii - 97;
            return ascii;
        }
        public int Alphabet_IgnoreCase(string key)
        {
            char c = Convert.ToChar(key.Substring(0, 1).ToLower());
            int ascii = Convert.ToInt32(c);
            //return ascii - 97;
            return ascii;
        }
        public int Small_weight(string key,int Partition_size=10)
        {
            int sum = 0;
            foreach (char c in key)
            {
                sum += Convert.ToInt32(c);
            }
            return sum % Partition_size;
        }
        public int Big_weight(string key, int Partition_size = 20)
        {
            int sum = 0;
            foreach (char c in key)
            {
                sum += Convert.ToInt32(c);
            }
            return sum % Partition_size;
        }
    }
}
