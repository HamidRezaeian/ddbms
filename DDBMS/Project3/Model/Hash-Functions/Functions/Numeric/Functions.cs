﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model.Hash_Functions.Functions.Numeric
{
    class Functions
    {
        public int EvenOdd(string key)
        {
            int i = Convert.ToInt32(key);
            return ((i % 2)==0) ? 1 : 0;
        }
        public int TenPart(string key)
        {
            return Convert.ToInt32(key.Substring(key.Length - 1, 1));
        }
    }
}
