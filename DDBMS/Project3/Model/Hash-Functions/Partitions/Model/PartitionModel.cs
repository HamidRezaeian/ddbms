﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model.Hash_Functions.Partitions.Model
{
    public class PartitionModel
    {
        private List<int> _Tuples = new List<int>();
        public List<int> Tuples
        {
            get
            {
                return _Tuples;
            }
            set
            {
                _Tuples = value;
            }
        }
    }
}
