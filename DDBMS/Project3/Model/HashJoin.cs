﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model
{
    public class HashJoin
    {
        #region Public
        
        public Double _HashJoin()
        {
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            //--------------------------------------------------------------------------------------

            Project3.Model.Dataset.StaticData.StaticData.Result.Clear();

            Project3.Model.Dataset.Keys.Creator keycreator = new Dataset.Keys.Creator(); 
            List<string> key1 = new List<string>();
            List<string> key2 = new List<string>();

            keycreator.Create(out key1,out key2); //Step1
            //========================================================
            List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel> partition1 = new List<Hash_Functions.Partitions.Model.PartitionModel>();
            List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel> partition2 = new List<Hash_Functions.Partitions.Model.PartitionModel>();

            Partitioning(key1, key2, out partition1, out partition2); //Step2
            //========================================================
            //Project3.Model.Dataset.StaticData.StaticData.Result = join(partition1, partition2);
            Project3.Model.Dataset.StaticData.StaticData.Result = RemoveDuplicates(join(partition1, partition2));
            //-------------------------------------------------------------------------------------------------------------
            watch.Stop();
            return watch.Elapsed.TotalMilliseconds;
        }
        #endregion
        #region Private
        private List<Project3.Model.Dataset.Models.ItemsModel> RemoveDuplicates(List<Project3.Model.Dataset.Models.ItemsModel> ResultDataset)
        {
            for (int i = 0; i < ResultDataset.Count; i++)
            {
                for (int i1 = 0; i1 < Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1.Count; i1++)
                {
                    ResultDataset[i].Items.RemoveAt((Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1[i1])-i1);
                }
                
            }
            return ResultDataset;
        }
        private List<Project3.Model.Dataset.Models.ItemsModel> join(List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel> partition1, List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel> partition2)
        {
            int n = 1;
            List<Project3.Model.Dataset.Models.ItemsModel> result_Dataset = new List<Dataset.Models.ItemsModel>();
            result_Dataset.Add(new Project3.Model.Dataset.Models.ItemsModel());
            result_Dataset[0].Items = new List<string>();
            foreach (var item in Project3.Model.Dataset.StaticData.StaticData.Datasets[0][0].Items)
            {
                result_Dataset[0].Items.Add(item);
            }
            foreach (var item in Project3.Model.Dataset.StaticData.StaticData.Datasets[1][0].Items)
            {
                result_Dataset[0].Items.Add(item);
            }

            for (int i = 0; i < partition1.Count; i++)
            {
                for (int i1 = 0; i1 < partition1[i].Tuples.Count; i1++)
                {
                    if (partition2.Count > i)
                    {
                        for (int i2 = 0; i2 < partition2[i].Tuples.Count; i2++)
                        {
                            if (CheckCondition(partition1[i].Tuples[i1], partition2[i].Tuples[i2]))
                            {
                                result_Dataset.Add(new Project3.Model.Dataset.Models.ItemsModel());
                                result_Dataset[n].Items = new List<string>();
                                foreach (var item in Project3.Model.Dataset.StaticData.StaticData.Datasets[0][(partition1[i].Tuples[i1]) + 1].Items)
                                {
                                    result_Dataset[n].Items.Add(item);
                                }
                                foreach (var item in Project3.Model.Dataset.StaticData.StaticData.Datasets[1][(partition2[i].Tuples[i2]) + 1].Items)
                                {
                                    result_Dataset[n].Items.Add(item);
                                }
                                n++;
                            }
                        }
                    }
                    else break;
                }
            }
            return result_Dataset;
        }
        private bool CheckCondition(int index_tuple1,int index_tuple2)
        {
            index_tuple1++; index_tuple2++;
            for (int i = 0; i < Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1.Count; i++)
            {
                //if (Project3.Model.Dataset.StaticData.StaticData.KeyIndex[0].Type == "String" && Project3.Model.Dataset.StaticData.StaticData.StringConfig == "Alphabet2")
                //{
                    string s1 = Project3.Model.Dataset.StaticData.StaticData.Datasets[0][index_tuple1].Items[Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1[i]];
                    string s2 = Project3.Model.Dataset.StaticData.StaticData.Datasets[1][index_tuple2].Items[Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD2[i]];
                    if (string.Compare(s1, s2, true) != 0)
                        return false;
                //}
                //else {
                //    if (Project3.Model.Dataset.StaticData.StaticData.Datasets[0][index_tuple1].Items[Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1[i]] != Project3.Model.Dataset.StaticData.StaticData.Datasets[1][index_tuple2].Items[Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD2[i]])
                //        return false;
                //}
            }
            return true;
        }

        //-----------Create Bucket
        private void Partitioning(List<string> key1, List<string> key2,out List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel> partition1, out List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel> partition2)
        {
            List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel> temp1 = new List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel>();
            List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel> temp2 = new List<Project3.Model.Hash_Functions.Partitions.Model.PartitionModel>();
            

            for (int i = 0; i < key1.Count; i++)
            {
                int index = HashFunction(key1[i]);
                while (index > temp1.Count-1)
                {
                    temp1.Add(new Hash_Functions.Partitions.Model.PartitionModel());
                }
                temp1[index].Tuples.Add(i);
            }
            for (int i = 0; i < key2.Count; i++)
            {
                int index = HashFunction(key2[i]);
                while (index > temp2.Count - 1)
                {
                    temp2.Add(new Hash_Functions.Partitions.Model.PartitionModel());
                }
                temp2[index].Tuples.Add(i);
            }
            partition1 = temp1;
            partition2 = temp2;
        }
        private int HashFunction(string key)
        {
            Project3.Model.Hash_Functions.Functions.String.Functions StringFunc = new Hash_Functions.Functions.String.Functions();
            Project3.Model.Hash_Functions.Functions.Numeric.Functions NumericFunc = new Hash_Functions.Functions.Numeric.Functions();

            if (Project3.Model.Dataset.StaticData.StaticData.KeyIndex[0].Type == "String")
            {
                switch (Project3.Model.Dataset.StaticData.StaticData.StringConfig)
                {
                    case "Alphabet":
                        return (StringFunc.Alphabet(key));
                    case "Alphabet2":
                        return (StringFunc.Alphabet_IgnoreCase(key));
                    case "SmallWeight":
                        return (StringFunc.Small_weight(key));
                    case "BigWeight":
                        return (StringFunc.Big_weight(key));
                    default:
                        return (StringFunc.Alphabet_IgnoreCase(key));
                }
            }
            else
            {
                switch (Project3.Model.Dataset.StaticData.StaticData.NumericConfig)
                {
                    case "EvenOdd":
                        return (NumericFunc.EvenOdd(key));
                    case "TenPart":
                        return (NumericFunc.TenPart(key));
                    default:
                        return (NumericFunc.TenPart(key));
                }
            }
        }
        #endregion
    }
}
