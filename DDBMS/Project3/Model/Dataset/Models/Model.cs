﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model.Dataset.Models
{
    public class Model
    {
        public string FieldName { get; set; }
        private List<string> _Rows = new List<string>();
        public List<string> Rows
        {
            get { return _Rows; }
            set { _Rows = value; }
        }
        public Model()
        {
        }
    }
}
