﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model.Dataset.Keys.Model
{
    public class KeyModel
    {
        public int Index { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public KeyModel()
        {

        }
    }
}
