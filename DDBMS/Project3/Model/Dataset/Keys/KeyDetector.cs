﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace DDBMS.Project3.Model.Dataset.Keys
{
    class KeyDetector
    {
        public Project3.Model.Dataset.Keys.Model.KeyModel[] Detect()
        {
            Project3.Model.Dataset.Keys.Model.KeyModel[] tempkeymodel = new Model.KeyModel[2];
            int[] tempindex = new int[2];
            tempindex=FindDuplicate((Project3.Model.Dataset.StaticData.StaticData.Datasets[0])[0].Items, (Project3.Model.Dataset.StaticData.StaticData.Datasets[1])[0].Items);
            string type = TrueType(Project3.Model.Dataset.StaticData.StaticData.Datasets[0], Project3.Model.Dataset.StaticData.StaticData.Datasets[1], tempindex[0], tempindex[1]);
            tempkeymodel[0] = new Model.KeyModel {
                Index = tempindex[0],
                Key = ((Project3.Model.Dataset.StaticData.StaticData.Datasets[0])[0].Items)[tempindex[0]],
                Type = type
            };
            tempkeymodel[1] = new Model.KeyModel
            {
                Index = tempindex[1],
                Key = ((Project3.Model.Dataset.StaticData.StaticData.Datasets[1])[0].Items)[tempindex[1]],
                Type = type
            };
            return tempkeymodel;
        }
        private int[] FindDuplicate(List<string> columnD1, List<string> columnD2)
        {
            int[] index =new int[2];
            Project3.Model.Dataset.StaticData.StaticData.KeyFound = false;
            Project3.Model.Dataset.StaticData.StaticData.KeyIsNull = true;
            Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1.Clear();
            Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD2.Clear();
            for (int i = 0; i < columnD1.Count; i++)
            {
                for (int i1 = 0; i1 < columnD2.Count; i1++)
                {
                    if (string.Compare(columnD1[i], columnD2[i1] ,true)==0)
                    {
                        Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1.Add(i);
                        Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD2.Add(i1);
                        Project3.Model.Dataset.StaticData.StaticData.KeyFound = true;

                        if(Project3.Model.Dataset.StaticData.StaticData.KeyIsNull)
                        if (!CheckNull(i, i1))
                        {
                            index[0] = i;
                            index[1] = i1;
                            Project3.Model.Dataset.StaticData.StaticData.KeyIsNull = false;
                            //goto end;
                        }

                    }
                    
                }
            }
            //end:
            return index;
        }
        private bool CheckNull(int index1,int index2)
        {
            for (int i = 0; i < Project3.Model.Dataset.StaticData.StaticData.Datasets[0].Count; i++)
            {
                if (Project3.Model.Dataset.StaticData.StaticData.Datasets[0][i].Items[index1] == "") return true;
            }
            for (int i = 0; i < Project3.Model.Dataset.StaticData.StaticData.Datasets[1].Count; i++)
            {
                if (Project3.Model.Dataset.StaticData.StaticData.Datasets[1][i].Items[index2] == "") return true;
            }
            return false;
        }
        private string FindType(List<Project3.Model.Dataset.Models.ItemsModel> Dataset,int Keyindex)
        {
            int n;
            for (int i = 1; i < Dataset.Count; i++)
            {
                if (Dataset[i].Items[Keyindex]!="")
                if (!int.TryParse(Dataset[i].Items[Keyindex], out n))
                {
                    return "String";
                }
            }
            return "Numeric";
        } 
        private string TrueType(List<Project3.Model.Dataset.Models.ItemsModel> Dataset1, List<Project3.Model.Dataset.Models.ItemsModel> Dataset2, int Keyindex1, int Keyindex2)
        {
            string Type1 = FindType(Dataset1, Keyindex1);
            string Type2 = FindType(Dataset2, Keyindex2); 
            return (Type1 == "Numeric" && Type2== "Numeric") ? "Numeric" : "String";
        }
    }
}
