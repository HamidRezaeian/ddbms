﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model.Dataset.Keys
{
    class Creator
    {
        public void Create(out List<string> Key1, out List<string> Key2)
        {
            List<string>[] temp = new List<string>[2];
            temp[0] = new List<string>();
            temp[1] = new List<string>();

            for (int i = 1; i < Project3.Model.Dataset.StaticData.StaticData.Datasets[0].Count; i++)
            {
                temp[0].Add(Project3.Model.Dataset.StaticData.StaticData.Datasets[0][i].Items[Project3.Model.Dataset.StaticData.StaticData.KeyIndex[0].Index]);
            }

            for (int i = 1; i < Project3.Model.Dataset.StaticData.StaticData.Datasets[1].Count; i++)
            {
                temp[1].Add(Project3.Model.Dataset.StaticData.StaticData.Datasets[1][i].Items[Project3.Model.Dataset.StaticData.StaticData.KeyIndex[1].Index]);
            }
            Key1 = temp[0];
            Key2 = temp[1];
        }
    }
}
