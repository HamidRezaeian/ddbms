﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DDBMS.Project3.Model.Dataset
{
    class DatasetManagement
    {
        public bool Build_Dataset(UIElementCollection StackpanelData,DataGrid _datagrid,int Dataset_index)
        {
            _datagrid.Columns.Clear();
            _datagrid.Items.Clear();
            _datagrid.Items.Refresh();
            //---------------------------------------------------------------------
            Project3.Model.Dataset.Converter converter = new Model.Dataset.Converter();
            List<Project3.Model.Dataset.Models.Model> temp = new List<Project3.Model.Dataset.Models.Model>();
            foreach (Project3.View.Sub_View.Add_Dataset.New.ObjectModel item in StackpanelData)
            {
                if (item.TextBox.Text.Trim() != "")
                {
                    string[] tempsplit = item.TextBox.Text.Split('=');
                    if (tempsplit.Count()==2)
                        if(tempsplit[1]!="")
                          temp.Add(new Model.Dataset.Models.Model() { FieldName = tempsplit[0], Rows = tempsplit[1].Split(',').ToList() });
                }

            }
            
            if (temp.Count != 0)
            {
                _datagrid = converter.Convert_ItemsModelToDataGrid(_datagrid, converter.Convert_ModelToItemsModel(temp));
                Project3.Model.Dataset.StaticData.StaticData.Datasets[Dataset_index] = converter.Convert_ModelToItemsModel(temp);
                Project3.Model.Dataset.StaticData.StaticData.DatasetsColumnModel[Dataset_index] =temp;
                return true;
            }
            else
            {
                _datagrid.Columns.Clear();
                _datagrid.Items.Clear();
                _datagrid.Items.Refresh();
                return false;
            }

        }
        public bool Save(int Dataset_index)
        {
            string temp="";
            foreach (var item in Project3.Model.Dataset.StaticData.StaticData.DatasetsColumnModel[Dataset_index])
            {
                temp +=string.Format("\n{0}=", item.FieldName);
                foreach (var item1 in item.Rows)
                {
                    temp += item1+",";
                }
                temp = temp.Substring(0, temp.Length - 1);
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Dataset file (*.data)|*.data";
            if (saveFileDialog.ShowDialog() == true)
            {
                File.WriteAllText(saveFileDialog.FileName, temp);
                return true;
            }
            return false;
        }

        public bool Load(StackPanel _stackpanel)
        {
            
            try
            {
                OpenFileDialog opendialog = new OpenFileDialog();
                opendialog.Filter = "Dataset file (*.data)|*.data";
                if (opendialog.ShowDialog() == true)
                {
                    string[] temp = File.ReadAllLines(opendialog.FileName);
                    _stackpanel.Children.Clear();
                    foreach (var item in temp)
                    {
                        if (item != "")
                        {
                            Project3.View.Sub_View.Add_Dataset.New.ObjectModel objModel = new View.Sub_View.Add_Dataset.New.ObjectModel();
                            objModel.TextBox.Text = item;
                            _stackpanel.Children.Add(objModel);
                        }

                    }


                }
                else return false;
                return true;
            }
            catch { return false; }
            
        }
    }
}
