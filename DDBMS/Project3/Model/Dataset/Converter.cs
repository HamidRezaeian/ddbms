﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace DDBMS.Project3.Model.Dataset
{
    class Converter
    {
        public List<Project3.Model.Dataset.Models.ItemsModel> Convert_ModelToItemsModel(List<Project3.Model.Dataset.Models.Model> columnData)
        {
            List<Project3.Model.Dataset.Models.ItemsModel> temp = new List<Project3.Model.Dataset.Models.ItemsModel>();
            foreach (var item in columnData)
            {
                if (temp.Count < 1) temp.Add(new Models.ItemsModel() {Items=new List<string>() {item.FieldName} });
                else temp[0].Items.Add(item.FieldName);
                for (int i = 1; i <= item.Rows.Count; i++)
                {
                    if (temp.Count <= i)
                        temp.Add(new Models.ItemsModel() { Items = new List<string>() { item.Rows[i-1] } });
                    else
                        temp[i].Items.Add(item.Rows[i-1]);
                }
            }
            return temp;
            //Project3.Model.HashJoin.Add_Dataset(temp);
        }
        public DataGrid Convert_ItemsModelToDataGrid(DataGrid _datagrid, List<Project3.Model.Dataset.Models.ItemsModel> _items)
        {
            //===================ColumnsAdd
            for (int i = 0; i < _items[0].Items.Count; i++)
            {
                _datagrid.Columns.Add(new DataGridTextColumn() { Header = _items[0].Items[i], Binding = new Binding(string.Format("Items[{0}]", i)), HeaderStyle = (Style)new FrameworkElement().FindResource("DataGridColumnHeaderStyle4")}) ;
            }
            _items.Remove(_items[0]);
            //===========================
            //====================RowsAdd
            foreach (var item in _items)
            {
                _datagrid.Items.Add(item);
            }
            //===========================
            return _datagrid;
        }

    }
}
