﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project3.Model.Dataset.StaticData
{
    public static class StaticData
    {
        #region Dataset Builder
        private static List<Project3.Model.Dataset.Models.ItemsModel> _Result = new List<Models.ItemsModel>();
        public static List<Project3.Model.Dataset.Models.ItemsModel> Result
        {
            get { return _Result; }
            set { _Result = value; }
        }

        private static List<Project3.Model.Dataset.Models.ItemsModel>[] _Datasets = new List<Models.ItemsModel>[2];
        public static List<Project3.Model.Dataset.Models.ItemsModel>[] Datasets
        {
            get { return _Datasets; }
            set { _Datasets = value; }
        }
        private static List<Project3.Model.Dataset.Models.Model>[] _DatasetsColumnModel = new List<Models.Model>[2];
        public static List<Project3.Model.Dataset.Models.Model>[] DatasetsColumnModel
        {
            get { return _DatasetsColumnModel; }
            set { _DatasetsColumnModel = value; }
        }
        #endregion
        #region Key Detection\Config
        public static List<int> index_DuplicateD1 = new List<int>();
        public static List<int> index_DuplicateD2 = new List<int>();
        public static bool KeyIsNull = false;
        public static bool KeyFound = false;
        private static Project3.Model.Dataset.Keys.Model.KeyModel[] _KeyIndex = new Keys.Model.KeyModel[2];
        public static Project3.Model.Dataset.Keys.Model.KeyModel[] KeyIndex
        {
            get
            {
                Project3.Model.Dataset.Keys.KeyDetector keyDetector = new Keys.KeyDetector();
                _KeyIndex=keyDetector.Detect();
                return _KeyIndex;
            }
            set { _KeyIndex = value; }
        }
        //--------------------------------Config-----------------------------
        public static string StringConfig = "Alphabet2";
        public static string NumericConfig = "TenPart";
        #endregion

    }
}
