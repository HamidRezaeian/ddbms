﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project3.View.Sub_View.Hash_Join
{
    /// <summary>
    /// Interaction logic for HashJoinUI.xaml
    /// </summary>
    public partial class HashJoinUI : UserControl
    {
        public HashJoinUI()
        {
            InitializeComponent();
            
            this.IsVisibleChanged += HashJoinUI_IsVisibleChanged;
        }
        Project3.Model.HashJoin hashjoin = new Model.HashJoin();
        private void HashJoinUI_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                Project3.Model.Dataset.Converter converter = new Model.Dataset.Converter();

                label_Duration.Content=string.Format("{0} ms", Convert.ToString(hashjoin._HashJoin()));
                label_key.Content = Project3.Model.Dataset.StaticData.StaticData.KeyIndex[0].Key;
                label_type.Content = Project3.Model.Dataset.StaticData.StaticData.KeyIndex[0].Type;
                label_StringConfig.Content = Project3.Model.Dataset.StaticData.StaticData.StringConfig;
                label_NumericConfig.Content = Project3.Model.Dataset.StaticData.StaticData.NumericConfig;
                label_Duplicate.Content = Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1.Count;

                label_Conditions.Content = "";
                foreach (var item in Project3.Model.Dataset.StaticData.StaticData.index_DuplicateD1)
                {
                    label_Conditions.Content =string.Format("{0},{1}", label_Conditions.Content,Project3.Model.Dataset.StaticData.StaticData.Datasets[0][0].Items[item]);
                }
                label_Conditions.Content = label_Conditions.Content.ToString().Substring(1);

                dataGrid.Columns.Clear();
                dataGrid.Items.Clear();
                dataGrid.Items.Refresh();
                //---------------------------------------------------------------------
                converter.Convert_ItemsModelToDataGrid(dataGrid, Project3.Model.Dataset.StaticData.StaticData.Result);
            }
        }
    }
}
