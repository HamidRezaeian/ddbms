﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project3.View.Sub_View.Hash_Function
{
    /// <summary>
    /// Interaction logic for HashFunctionUI.xaml
    /// </summary>
    public partial class HashFunctionUI : UserControl
    {
        public HashFunctionUI()
        {
            InitializeComponent();
            this.IsVisibleChanged += HashFunctionUI_IsVisibleChanged;
            //-------------------------------------------------------
            radioButton1string.Checked += RadioButton1string_Checked;
            radioButton2string.Checked += RadioButton2string_Checked;
            radioButton3string.Checked += RadioButton3string_Checked;
            radioButton4string.Checked += RadioButton4string_Checked;
            //-------------------------------------------------------
            radioButton1numeric.Checked += RadioButton1numeric_Checked;
            radioButton2numeric.Checked += RadioButton2numeric_Checked;
        }

        private void RadioButton2numeric_Checked(object sender, RoutedEventArgs e)
        {
            Project3.Model.Dataset.StaticData.StaticData.NumericConfig = "TenPart";
        }

        private void RadioButton1numeric_Checked(object sender, RoutedEventArgs e)
        {
            Project3.Model.Dataset.StaticData.StaticData.NumericConfig = "EvenOdd";
        }

        private void RadioButton4string_Checked(object sender, RoutedEventArgs e)
        {
            Project3.Model.Dataset.StaticData.StaticData.StringConfig = "BigWeight";
        }

        private void RadioButton3string_Checked(object sender, RoutedEventArgs e)
        {
            Project3.Model.Dataset.StaticData.StaticData.StringConfig = "SmallWeight";
        }

        private void RadioButton2string_Checked(object sender, RoutedEventArgs e)
        {
            Project3.Model.Dataset.StaticData.StaticData.StringConfig = "Alphabet2";
        }

        private void RadioButton1string_Checked(object sender, RoutedEventArgs e)
        {
            Project3.Model.Dataset.StaticData.StaticData.StringConfig = "Alphabet";
        }

        private void HashFunctionUI_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                Label_Key.Content = Project3.Model.Dataset.StaticData.StaticData.KeyIndex[0].Key;
                Label_KeyType.Content = Project3.Model.Dataset.StaticData.StaticData.KeyIndex[0].Type;
                Project3.View_Model.UI_Management.UIM_Main.IsFinished_HashFunction=check();
            }
        }
        private bool check()
        {
            if (!Project3.Model.Dataset.StaticData.StaticData.KeyFound)
            {
                BorderError.Visibility = Visibility.Visible;
                Label_Error.Content = "Error : Key Not Found !";
                Label_Key.Content = "[ Not Found! ]";
                Label_KeyType.Content = "[ Not Found! ]";
                return false;
            }
            else
            {
                if (Project3.Model.Dataset.StaticData.StaticData.KeyIsNull)
                {
                    BorderError.Visibility = Visibility.Visible;
                    Label_Error.Content = "Error : Key Is Null !";
                    return false;
                }
                else
                    BorderError.Visibility = Visibility.Collapsed;
            }
            return true;
        }
    }
}
