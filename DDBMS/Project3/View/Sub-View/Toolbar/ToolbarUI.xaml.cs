﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project3.View.Sub_View.Toolbar
{
    /// <summary>
    /// Interaction logic for ToolbarUI.xaml
    /// </summary>
    public partial class ToolbarUI : UserControl
    {
        public ToolbarUI()
        {
            InitializeComponent();
            Button_Dataset.Checked += Button_Dataset_Checked;
            Button_HashFunction.Checked += Button_HashFunction_Checked;
            Button_HashJoin.Checked += Button_HashJoin_Checked;
        }

        private void Button_HashJoin_Checked(object sender, RoutedEventArgs e)
        {
            Project3.View_Model.UI_Management.UIM_Main.SelectTab("Hashjoin");
        }

        private void Button_HashFunction_Checked(object sender, RoutedEventArgs e)
        {
            Project3.View_Model.UI_Management.UIM_Main.SelectTab("HashFunction");
        }

        private void Button_Dataset_Checked(object sender, RoutedEventArgs e)
        {
            Project3.View_Model.UI_Management.UIM_Main.SelectTab("Datasets");
        }
    }
}
