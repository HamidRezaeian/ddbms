﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project3.View.Sub_View.Add_Dataset
{
    /// <summary>
    /// Interaction logic for AddDatasetUI.xaml
    /// </summary>
    public partial class AddDatasetUI : UserControl
    {
        public AddDatasetUI()
        {
            InitializeComponent();
            Project3.View_Model.UI_Management.UIM_AddDataset.AddDataset = this;
            ButtonNew.Click += ButtonNew_Click;
            newUI.FillUI.ButtonDone.Click += ButtonDone_Click;
            successPanel.ButtonRefresh.Click += ButtonRefresh_Click;
        }

        private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            successPanel.Visibility = Visibility.Collapsed;
        }

        private void ButtonDone_Click(object sender, RoutedEventArgs e)
        {
            successPanel.Visibility = Visibility.Visible;
        }

        private void ButtonNew_Click(object sender, RoutedEventArgs e)
        {
            newUI.StackPanelObjModel.Children.Clear();
        }
    }
}
