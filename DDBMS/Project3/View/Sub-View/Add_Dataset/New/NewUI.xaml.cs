﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project3.View.Sub_View.Add_Dataset.New
{
    /// <summary>
    /// Interaction logic for NewUI.xaml
    /// </summary>
    public partial class NewUI : UserControl
    {
        public NewUI()
        {
            InitializeComponent();
            ButtonAddField.Click += ButtonAddField_Click;
            this.KeyDown += NewUI_KeyDown;

        }


        private void NewUI_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                add();
            }
        }


        private void ButtonAddField_Click(object sender, RoutedEventArgs e)
        {
            add();
        }
        private void add()
        {
            Project3.View.Sub_View.Add_Dataset.New.ObjectModel objModel = new ObjectModel();
            StackPanelObjModel.Children.Add(objModel);
            StackPanelObjModel.UpdateLayout();
            ScrollViewer_StackPanelObjModel.ScrollToEnd();
            StackPanelObjModel.MoveFocus(new TraversalRequest(FocusNavigationDirection.Last));
        }
    }
}
