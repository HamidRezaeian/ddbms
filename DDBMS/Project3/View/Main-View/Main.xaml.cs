﻿using DDBMS.Project3.View.Sub_View.Add_Dataset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project3.View.Main_View
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : UserControl
    {
        public Main()
        {
            InitializeComponent();

            DDBMS.Project3.View_Model.UI_Management.UIM_Main.main = this;
            Dataset1.newUI.ButtonNext.Click += ButtonNext_Click;
            Dataset2.newUI.ButtonNext.Click += ButtonNext_Click1;

            Dataset1.newUI.FillUI.ButtonSave.Click += ButtonSave_Click;
            Dataset2.newUI.FillUI.ButtonSave.Click += ButtonSave_Click1;

            Dataset1.ButtonLoad.Click += ButtonLoad_Click;
            Dataset2.ButtonLoad.Click += ButtonLoad_Click1;

            Dataset1.successPanel.IsVisibleChanged += SuccessPanel_IsVisibleChanged;
            Dataset2.successPanel.IsVisibleChanged += SuccessPanel_IsVisibleChanged1;
        }



        private void SuccessPanel_IsVisibleChanged1(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true && Dataset1.successPanel.Visibility == Visibility.Visible)
                DDBMS.Project3.View_Model.UI_Management.UIM_Main.IsFinished_Dataset = true;
            else
                DDBMS.Project3.View_Model.UI_Management.UIM_Main.IsFinished_Dataset = false;
        }

        private void SuccessPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true && Dataset2.successPanel.Visibility == Visibility.Visible)
                DDBMS.Project3.View_Model.UI_Management.UIM_Main.IsFinished_Dataset = true;
            else
                DDBMS.Project3.View_Model.UI_Management.UIM_Main.IsFinished_Dataset = false;
        }

        Project3.Model.Dataset.DatasetManagement datamanage = new Model.Dataset.DatasetManagement();
        private void ButtonLoad_Click1(object sender, RoutedEventArgs e)
        {
            if (datamanage.Load(Dataset2.newUI.StackPanelObjModel)) Dataset2.OnClick1StoryBoard_BeginStoryboard.Storyboard.Begin(); //Dataset2.newUI.Visibility = Visibility.Visible;
        }

        private void ButtonLoad_Click(object sender, RoutedEventArgs e)
        {
            if (datamanage.Load(Dataset1.newUI.StackPanelObjModel)) Dataset1.OnClick1StoryBoard_BeginStoryboard.Storyboard.Begin(); //Dataset1.newUI.Visibility = Visibility.Visible;
        }

        private void ButtonSave_Click1(object sender, RoutedEventArgs e)
        {
            datamanage.Save(1);
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            datamanage.Save(0);
        }

        private void ButtonNext_Click1(object sender, RoutedEventArgs e)
        {
            if (!datamanage.Build_Dataset(Dataset2.newUI.StackPanelObjModel.Children, Dataset2.newUI.FillUI.dataGrid, 1))
            {
                Dataset2.newUI.FillUI.ButtonDone.Visibility = Visibility.Collapsed;
                Dataset2.newUI.FillUI.ButtonSave.Visibility = Visibility.Collapsed;
            }
            else
            {
                Dataset2.newUI.FillUI.ButtonDone.Visibility = Visibility.Visible;
                Dataset2.newUI.FillUI.ButtonSave.Visibility = Visibility.Visible;
            }
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            if (!datamanage.Build_Dataset(Dataset1.newUI.StackPanelObjModel.Children, Dataset1.newUI.FillUI.dataGrid,0))
            {
                Dataset1.newUI.FillUI.ButtonDone.Visibility = Visibility.Collapsed;
                Dataset1.newUI.FillUI.ButtonSave.Visibility = Visibility.Collapsed;
            }
            else
            {
                Dataset1.newUI.FillUI.ButtonDone.Visibility = Visibility.Visible;
                Dataset1.newUI.FillUI.ButtonSave.Visibility = Visibility.Visible;
            }
        }

    }
}
