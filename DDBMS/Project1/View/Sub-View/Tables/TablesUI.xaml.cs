﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project1.View.Sub_View.Tables
{
    /// <summary>
    /// Interaction logic for TablesUI.xaml
    /// </summary>
    public partial class TablesUI : UserControl
    {
        DDBMS.Project1.Model.Database.Management manage = new Model.Database.Management();
        DDBMS.Project1.Model.Database.Read.Attach_Database attach = new Model.Database.Read.Attach_Database();
        public TablesUI()
        {
            InitializeComponent();
            Project1.View_Model.UI_Management.UIM_Tables.Tables = this;
            button_attach.Click += Button_attach_Click;
            button_detach.Click += Button_detach_Click;
        }

        private void Button_detach_Click(object sender, RoutedEventArgs e)
        {
            if (attach.detach(Project1.Model.Database.StaticData.StaticData.DatabaseName))
            {
                Project1.Model.Database.StaticData.StaticData.DatabaseName = "";
                Project1.Model.Database.StaticData.StaticData.DatabasePath = "";
                Project1.Model.Database.StaticData.StaticData.QueriesPath = "";
            }
        }

        private void Button_attach_Click(object sender, RoutedEventArgs e)
        {
            manage.ReadDatabase(Panel);
        }
    }
}
