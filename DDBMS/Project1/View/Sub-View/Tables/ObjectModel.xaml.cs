﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project1.View.Sub_View.Tables
{
    /// <summary>
    /// Interaction logic for ObjectModel.xaml
    /// </summary>
    public partial class ObjectModel : UserControl
    {
        public ObjectModel()
        {
            InitializeComponent();
            dataGrid.RowEditEnding += DataGrid_RowEditEnding;
            
        }

        private void DataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            SqlDataAdapter adabter = new SqlDataAdapter(string.Format("select * from {0}", (string)Title.Content), Project1.Model.Database.StaticData.StaticData.Connection);
            DataSet ds = new DataSet();

            SqlCommandBuilder builder = new SqlCommandBuilder(adabter);
            adabter.UpdateCommand = builder.GetUpdateCommand();
            //adabter.UpdateCommand.Connection = Project1.Model.Database.StaticData.StaticData.Connection;
            //adabter.Fill(ds, (string)Title.Content);
            
            DataTable dt = new DataTable((string)Title.Content);
            dt= ((DataView)dataGrid.ItemsSource).ToTable();
            //ds.Tables.Add(((DataView)dataGrid.ItemsSource).ToTable((string)Title.Content));

            adabter.InsertCommand = builder.GetInsertCommand();
            adabter.DeleteCommand = builder.GetDeleteCommand();
                    adabter.Update(dt);
                    
        }

        void test()
        {
            SqlDataAdapter adabter = new SqlDataAdapter("", Project1.Model.Database.StaticData.StaticData.Connection);
            
            DataSet ds = new DataSet();
            adabter.Fill(ds, "T1");

        }
    }
}
