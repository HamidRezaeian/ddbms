﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project1.View.Sub_View.toolbar
{
    /// <summary>
    /// Interaction logic for ToolbarUI.xaml
    /// </summary>
    public partial class ToolbarUI : UserControl
    {
        public ToolbarUI()
        {
            InitializeComponent();
            Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar = this;
            button_reload.Click += Button_reload_Click;
            RadioButton_Tables.Checked += RadioButton_Tables_Checked;
            RadioButton_SqlQueries.Checked += RadioButton_SqlQueries_Checked;
        }

        private void RadioButton_SqlQueries_Checked(object sender, RoutedEventArgs e)
        {
            Project1.View_Model.UI_Management.UIM_Main.main.QueriesUI.Visibility = Visibility.Visible;
            Project1.View_Model.UI_Management.UIM_Main.main.TablesUI.Visibility = Visibility.Collapsed;
            Project1.Model.Database.StaticData.StaticData.QueriesPath = Project1.Model.Database.StaticData.StaticData.QueriesPath;
        }

        private void RadioButton_Tables_Checked(object sender, RoutedEventArgs e)
        {
            Project1.View_Model.UI_Management.UIM_Main.main.QueriesUI.Visibility = Visibility.Collapsed;
            Project1.View_Model.UI_Management.UIM_Main.main.TablesUI.Visibility = Visibility.Visible;
            Project1.Model.Database.StaticData.StaticData.DatabasePath = Project1.Model.Database.StaticData.StaticData.DatabasePath;
        }

        Project1.Model.Database.Management manage = new Model.Database.Management();
        private void Button_reload_Click(object sender, RoutedEventArgs e)
        {
            manage.ReadDatabase(Project1.View_Model.UI_Management.UIM_Tables.Tables.Panel, Project1.Model.Database.StaticData.StaticData.DatabasePath);
        }
    }
}
