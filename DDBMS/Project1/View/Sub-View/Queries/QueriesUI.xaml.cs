﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ICSharpCode.AvalonEdit;
using System.IO;
using System.Xml;

namespace DDBMS.Project1.View.Sub_View.Queries
{
    /// <summary>
    /// Interaction logic for QueriesUI.xaml
    /// </summary>
    public partial class QueriesUI : UserControl
    {
        public QueriesUI()
        {
            InitializeComponent();
            Project1.View_Model.UI_Management.UIM_Queries.Queries = this;
            button_execute.Click += Button_execute_Click;
            button_save.Click += Button_save_Click;
            button_load.Click += Button_load_Click;
            AvalonTextBox.TextChanged += AvalonTextBox_TextChanged;
            AvalonTextBox.KeyDown += AvalonTextBox_KeyDown;
            button_sizep.Click += Button_sizep_Click;
            button_sizem.Click += Button_sizem_Click;
            using (Stream s = File.OpenRead(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + @"\Attachements\Others\SQLMode.xshd"))
            {
                using (XmlTextReader reader = new XmlTextReader(s))
                {
                    AvalonTextBox.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.Xshd.HighlightingLoader.Load
                        (reader, ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance);
                }
            }
        }

        private void AvalonTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
            {
                string query = (AvalonTextBox.SelectedText.Length > 0) ? AvalonTextBox.SelectedText : AvalonTextBox.Text;
                if (manage.Save(query, Project1.Model.Database.StaticData.StaticData.QueriesPath)) Project1.Model.Database.StaticData.StaticData.IsQuerySaved = true;
                e.Handled = true;
            }
        }

        private void Button_sizem_Click(object sender, RoutedEventArgs e)
        {
            if (AvalonTextBox.FontSize>3)
            AvalonTextBox.FontSize--;
        }

        private void Button_sizep_Click(object sender, RoutedEventArgs e)
        {
            AvalonTextBox.FontSize++;
        }
        private void AvalonTextBox_TextChanged(object sender, EventArgs e)
        {
            Project1.Model.Database.StaticData.StaticData.IsQuerySaved = false;
        }

        Project1.Model.Database.Management manage = new Model.Database.Management();
        private void Button_load_Click(object sender, RoutedEventArgs e)
        {
            string temp = "";
            if (manage.Load(out temp))AvalonTextBox.Text= temp;
        }

        private void Button_save_Click(object sender, RoutedEventArgs e)
        {
            string query = (AvalonTextBox.SelectedText.Length > 0) ? AvalonTextBox.SelectedText : AvalonTextBox.Text;
            if (manage.Save(query)) Project1.Model.Database.StaticData.StaticData.IsQuerySaved = true;
        }

        
        private void Button_execute_Click(object sender, RoutedEventArgs e)
        {
            string error = "";
            string query = (AvalonTextBox.SelectedText.Length>0)? AvalonTextBox.SelectedText: AvalonTextBox.Text;
            
            if (query != "")
                if (!manage.Execute(query, dataGrid, out error))
                {
                    if (error == "Cannot find table 0.")
                    {
                        BorderError.Visibility = Visibility.Collapsed;
                        BorderChange.Visibility = Visibility.Visible;
                        dataGrid.Visibility = Visibility.Collapsed;
                        labelResult.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        LabelError.Content = error;
                        BorderError.Visibility = Visibility.Visible;
                        BorderChange.Visibility = Visibility.Collapsed;
                        dataGrid.Visibility = Visibility.Collapsed;
                        labelResult.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    LabelError.Content = error;
                    BorderError.Visibility = Visibility.Collapsed;
                    BorderChange.Visibility = Visibility.Collapsed;
                    dataGrid.Visibility = Visibility.Visible;
                    labelResult.Visibility = Visibility.Visible;
                }
        }
    }
}
