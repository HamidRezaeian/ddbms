﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DDBMS.Project1.Model.Database.StaticData
{
    public static class StaticData
    {
        public static SqlConnection Connection = new SqlConnection();
        public static void ConnectionSet(string database_name = "")
        {
            string ConnectionString = "";
            if (database_name == "")
            {
                ConnectionString = string.Format(@"Data Source=.;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Pooling=false");
            }
            else
            {
                ConnectionString = string.Format(@"Data Source=.;Initial Catalog={0};Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Pooling=false", database_name);
            }


            Project1.Model.Database.StaticData.StaticData.Connection = new System.Data.SqlClient.SqlConnection(ConnectionString);
        }
        private static string _DatabaseName = "";
        public static string DatabaseName
        {
            get
            {
                return _DatabaseName;
            }
            set
            {
                _DatabaseName = value;
                if (value=="")
                {
                    Project1.View_Model.UI_Management.UIM_Tables.Tables.Panel.Children.Clear();
                    Project1.View_Model.UI_Management.UIM_Tables.Tables.button_detach.Visibility = System.Windows.Visibility.Collapsed;
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.Border_Reload.Visibility = System.Windows.Visibility.Collapsed;
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_SqlQueries.IsEnabled = false;
                    Project1.View_Model.UI_Management.UIM_Main.main.QueriesUI.Visibility = System.Windows.Visibility.Collapsed;
                    Project1.View_Model.UI_Management.UIM_Main.main.TablesUI.Visibility = System.Windows.Visibility.Visible;

                }
                else
                {
                    Project1.View_Model.UI_Management.UIM_Tables.Tables.button_detach.Visibility = System.Windows.Visibility.Visible;
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.Border_Reload.Visibility = System.Windows.Visibility.Visible;
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_SqlQueries.IsEnabled = true;
                    //Project1.View_Model.UI_Management.UIM_Queries.Queries.AvalonTextBox.Text = "";
                    Project1.View_Model.UI_Management.UIM_Queries.Queries.dataGrid.ItemsSource = null;
                    Project1.View_Model.UI_Management.UIM_Queries.Queries.dataGrid.Visibility = System.Windows.Visibility.Collapsed;
                    Project1.View_Model.UI_Management.UIM_Queries.Queries.labelResult.Visibility = System.Windows.Visibility.Visible;
                    Project1.View_Model.UI_Management.UIM_Queries.Queries.BorderChange.Visibility = System.Windows.Visibility.Collapsed;
                    Project1.View_Model.UI_Management.UIM_Queries.Queries.BorderError.Visibility = System.Windows.Visibility.Collapsed;

                }

            }
        }
        private static string _DatabasePath = "";
        public static string DatabasePath
        {
            get
            {
                return _DatabasePath;
            }
            set
            {
                _DatabasePath = value;
                if (Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_Tables.IsChecked.Value)
                {
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_SqlQueries.Content = string.Format("SQL Queries");

                    //Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.label_address.Content=string.Format("{0}", value);
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_Tables.Content = string.Format("Tables {0}", value);
                }

            }
        }

        public static List<string> TablesName = new List<string>();

        private static string _QueriesPath = "";
        public static string QueriesPath
        {
            get
            {
                return _QueriesPath;
            }
            set
            {
                _QueriesPath = value;
                
                if (Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_SqlQueries.IsChecked.Value)
                {
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_Tables.Content = string.Format("Tables");
                    if (IsQuerySaved)
                        //Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.label_address.Content= string.Format("{0}", value);
                        Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_SqlQueries.Content = string.Format("SQL Queries {0}", value);
                    else
                        if (value != "")
                        //Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.label_address.Content = string.Format("{0} *", value);
                        Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_SqlQueries.Content = string.Format("SQL Queries {0} *", value);
                }
            }
        }


        private static bool _IsQuerySaved = true;
        public static bool IsQuerySaved
        {
            get
            {
                return _IsQuerySaved;
            }
            set
            {
                _IsQuerySaved = value;
                if (value==true)
                    //Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.label_address.Content = QueriesPath;
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_SqlQueries.Content = string.Format("SQL Queries {0}", QueriesPath);

                else
                    if (QueriesPath!="")
                    //Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.label_address.Content = QueriesPath+" *";
                    Project1.View_Model.UI_Management.UIM_Toolbar.Toolbar.RadioButton_SqlQueries.Content = string.Format("SQL Queries {0} *", QueriesPath);

            }
        }
    }
}
