﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project1.Model.Database.Read
{
    class Read
    {
        public List<DataTable> Read_Database(string mdf_path)
        {
            Project1.Model.Database.StaticData.StaticData.DatabasePath = mdf_path;
            Project1.Model.Database.Read.Attach_Database attach = new Attach_Database();
            string database_name = "";
            attach.AttachDatabase(mdf_path,out database_name);
            Project1.Model.Database.StaticData.StaticData.ConnectionSet(database_name);
            Project1.Model.Database.Read.RetriveTables retrive = new RetriveTables();

            

            return retrive.Fill(database_name);
        }
    }
}
