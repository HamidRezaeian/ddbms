﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project1.Model.Database.Read
{
    class RetriveTables
    {
        public List<DataTable> Fill(string database_name)
        {
            
            DataTable Tables = RetriveTableNames(database_name);
            List<DataTable> temp = new List<DataTable>();
            Project1.Model.Database.StaticData.StaticData.TablesName.Clear();
            foreach (var item in Tables.Rows)
            {
                string tablename =(string)((DataRow)item).ItemArray[0];
                string q = string.Format(@"SELECT * from {0}", tablename);
                Project1.Model.Database.StaticData.StaticData.TablesName.Add((string)(tablename));
                SqlDataAdapter adabter = new SqlDataAdapter(q, Project1.Model.Database.StaticData.StaticData.Connection);
                DataSet ds = new DataSet();
                adabter.Fill(ds, tablename);
                temp.Add(ds.Tables[0]);
            }
            
            return temp;
        }
        private DataTable RetriveTableNames(string database_name)
        {
            
            string q = string.Format(@"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG = '{0}'", database_name);
            SqlDataAdapter adabter = new SqlDataAdapter(q, Project1.Model.Database.StaticData.StaticData.Connection);
            DataSet ds = new DataSet();
            adabter.Fill(ds, "T1");
            
            return ds.Tables[0];
        }

    }
}
