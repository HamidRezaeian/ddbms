﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace DDBMS.Project1.Model.Database.Read
{
    class Attach_Database
    {
      
        public bool AttachDatabase(string mdf_path,out string name)
        {
           
            string database_name = dbname(mdf_path);
            name = database_name;
            StaticData.StaticData.DatabaseName = name;
            try
            {
                //detach(database_name);
                //SqlConnection.ClearAllPools();
                attach(mdf_path, database_name);
                return true;
            }
            catch
            {
                return false;
            }
            
        }
        private string dbname(string mdf_path)
        {
           string[] temp= mdf_path.Split('\\');
            return (temp[temp.Count() - 1]).Substring(0, temp[temp.Count() - 1].Length - 4);
        }
        public bool attach(string mdf_path, string database_name)
        {
            Project1.Model.Database.StaticData.StaticData.ConnectionSet();
            SqlCommand cmd = new SqlCommand("", Project1.Model.Database.StaticData.StaticData.Connection);
            string ldf_path = string.Format("{0}_log.ldf", mdf_path.Substring(0, mdf_path.Length - 4));
            try
            {
                try
                {
                    cmd.CommandText = string.Format(@"exec sys.sp_attach_db    {0},    '{1}',    '{2}'",database_name,mdf_path, ldf_path);
                    Project1.Model.Database.StaticData.StaticData.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    Project1.Model.Database.StaticData.StaticData.Connection.Dispose();
                }
                catch(Exception e)
                {
                    //MessageBox.Show(e.Message);
                    cmd.CommandText = string.Format(@"exec sys.sp_attach_db    {0},    '{1}'", database_name, mdf_path);
                    Project1.Model.Database.StaticData.StaticData.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    Project1.Model.Database.StaticData.StaticData.Connection.Dispose();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool detach(string database_name)
        {
            try {
                Project1.Model.Database.StaticData.StaticData.ConnectionSet();
                SqlCommand cmd = new SqlCommand("", Project1.Model.Database.StaticData.StaticData.Connection);
                //cmd.CommandText = string.Format("DROP DATABASE {0}", database_name);
                cmd.CommandText = string.Format(@"exec sys.sp_detach_db    {0}", database_name);
                Project1.Model.Database.StaticData.StaticData.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                Project1.Model.Database.StaticData.StaticData.Connection.Dispose();
                return true;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

    }
}
