﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.IO;

namespace DDBMS.Project1.Model.Database
{
    class Management
    {
        public void ReadDatabase(UniformGrid parent_panel,string path="")
        {
            try
            {
                Project1.View_Model.UI_Management.UIM_Tables.Tables.Error_Border.Visibility = System.Windows.Visibility.Collapsed;
                SqlConnection.ClearAllPools();
                Project1.Model.Database.Read.Read read = new Read.Read();
                string mdf_path = path;
                if (path == "")
                {
                    OpenFileDialog opendialog = new OpenFileDialog();
                    opendialog.ValidateNames = false;
                    opendialog.Filter = "Database file (*.mdf)|*.mdf";
                    if (opendialog.ShowDialog() == true)
                    {
                        parent_panel.Children.Clear();
                        mdf_path = opendialog.FileName;
                        int n = 0;
                        foreach (var item in read.Read_Database(mdf_path))
                        {
                            DDBMS.Project1.View.Sub_View.Tables.ObjectModel table = new View.Sub_View.Tables.ObjectModel();
                            table.dataGrid.ItemsSource = item.DefaultView;
                            table.Title.Content = Project1.Model.Database.StaticData.StaticData.TablesName[n++];
                            table.Border.Background = Project1.Model.Public.RandomColor.Random_color();
                            table.Border.BorderBrush = Project1.Model.Public.RandomColor.Random_color();
                            parent_panel.Children.Add(table);
                        }
                    }
                }
                else
                {
                    parent_panel.Children.Clear();
                    int n = 0;
                    foreach (var item in read.Read_Database(mdf_path))
                    {
                        DDBMS.Project1.View.Sub_View.Tables.ObjectModel table = new View.Sub_View.Tables.ObjectModel();
                        table.dataGrid.ItemsSource = item.DefaultView;
                        table.Title.Content = Project1.Model.Database.StaticData.StaticData.TablesName[n++];
                        table.Border.Background = Project1.Model.Public.RandomColor.Random_color();
                        table.Border.BorderBrush = Project1.Model.Public.RandomColor.Random_color();
                        parent_panel.Children.Add(table);
                    }
                }
            }
            catch(Exception e)
            {
                Project1.View_Model.UI_Management.UIM_Tables.Tables.Error_Border.Visibility = System.Windows.Visibility.Visible;
                Project1.View_Model.UI_Management.UIM_Tables.Tables.label_Error.Content = e.Message;
            }
        }

        public bool Execute(string query,DataGrid datagrid,out string Error)
        {
            Error = "";
            try {
                SqlDataAdapter adabter = new SqlDataAdapter(@query, Project1.Model.Database.StaticData.StaticData.Connection);
                DataSet ds = new DataSet();
                adabter.Fill(ds, "T1");

                datagrid.ItemsSource = ds.Tables[0].DefaultView;
                return true;
            }
            catch(Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool Save(string query,string path="")
        {
            if (path == "")
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "SQL Query file (*.sql)|*.sql";
                if (saveFileDialog.ShowDialog() == true)
                {
                    Project1.Model.Database.StaticData.StaticData.QueriesPath = saveFileDialog.FileName;
                    File.WriteAllText(Project1.Model.Database.StaticData.StaticData.QueriesPath, query);
                    return true;
                }
            }
            else
            {
                File.WriteAllText(path, query);
                return true;
            }
            return false;
        }

        public bool Load(out string s)
        {

            try
            {
                OpenFileDialog opendialog = new OpenFileDialog();
                opendialog.Filter = "SQL Query file (*.sql)|*.sql";
                if (opendialog.ShowDialog() == true)
                {
                    Project1.Model.Database.StaticData.StaticData.QueriesPath = opendialog.FileName;
                    s= File.ReadAllText(Project1.Model.Database.StaticData.StaticData.QueriesPath);
                    Project1.Model.Database.StaticData.StaticData.IsQuerySaved = true;
                    return true;
                }
                s = "";
                return false;
            }
            catch { Project1.Model.Database.StaticData.StaticData.QueriesPath = "";s = ""; return false; }

        }
    }
}
