﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project1.Model.Public
{
    class Unload
    {
        public void unload()
        {
            SqlConnection.ClearAllPools();
            Project1.Model.Database.StaticData.StaticData.Connection = new SqlConnection();
            Project1.Model.Database.StaticData.StaticData.DatabaseName = "";
            Project1.Model.Database.StaticData.StaticData.DatabasePath = "";
            Project1.Model.Database.StaticData.StaticData.TablesName = new List<string>();
            Project1.Model.Database.StaticData.StaticData.QueriesPath = "";
            Project1.Model.Database.StaticData.StaticData.IsQuerySaved = true;
        }
    }
}
