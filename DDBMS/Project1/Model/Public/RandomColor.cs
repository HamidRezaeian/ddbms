﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DDBMS.Project1.Model.Public
{
    public class RandomColor
    {
        public static Brush Random_color(int maxr=200,int maxg=150,int maxb=150)
        {
            Thread.Sleep(10);
            Random r = new Random();
            Brush brush = new SolidColorBrush(Color.FromArgb(220,(byte)r.Next(1, maxr),
              (byte)r.Next(1, maxg), (byte)r.Next(1, maxb)));
            return brush;
        }
    }
}
