﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace DDBMS.Public.View_Model.UI_Management
{
    public static class UIM_WindowUI
    {
        private static Public.View.Sub_View.WindowUI.WindowUI _windowUI = new View.Sub_View.WindowUI.WindowUI();
        public static Public.View.Sub_View.WindowUI.WindowUI windowUI
        {
            get
            {
                return _windowUI;
            }
            set
            {
                _windowUI = value;
            }
        }
        public static void Open(string project)
        {
            switch (project)
            {
                case "Project1":
                    HideAll();
                    TabAdd(project);
                    if (!CheckContentVisibility(project))
                    {
                        Project1.View.Main_View.Main main = new Project1.View.Main_View.Main();
                    try {
                            windowUI.Content.Children.Add(main);
                        }
                        catch(Exception e)
                        {
                            MessageBox.Show(e.Message);
                        }
                    }
                    break;
                case "Project2":
                    HideAll();
                    TabAdd(project);
                    if (!CheckContentVisibility(project))
                    {
                        Project2.View.Main_View.Main main = new Project2.View.Main_View.Main();
                        windowUI.Content.Children.Add(main);
                    }
                    break;
                case "Project3":
                    HideAll();
                    TabAdd(project);
                    if (!CheckContentVisibility(project))
                    {
                        Project3.View.Main_View.Main main = new Project3.View.Main_View.Main();
                        windowUI.Content.Children.Add(main);
                    }
                    break;
                case "Home":
                    HideAll();
                    TabAdd(project);
                    if (!CheckContentVisibility(project))
                    {
                        Public.View.Sub_View.Home.HomeUI home = new View.Sub_View.Home.HomeUI();
                        windowUI.Content.Children.Add(home);
                    }
                    break;
            }
        }
        private static bool CheckContentVisibility(string project)
        {
            foreach (UserControl item in windowUI.Content.Children)
            {
                if (item.Name == project)
                {
                    item.Visibility = System.Windows.Visibility.Visible;
                    return true;
                }
            }
            return false;
        }
        private static void HideAll()
        {
            foreach (UserControl item in windowUI.Content.Children)
            {
                item.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private static void TabAdd(string project)
        {
            Brush black = new SolidColorBrush(Color.FromArgb((byte)255, (byte)0, (byte)0, (byte)0));
            Brush white = new SolidColorBrush(Color.FromArgb((byte)255, (byte)255, (byte)255, (byte)255));
            Brush noColor= new SolidColorBrush(Color.FromArgb((byte)0, (byte)0, (byte)0, (byte)0));

            switch (project)
            {
                case "Project1":
                    windowUI.Tab_Proj1.Visibility = System.Windows.Visibility.Visible;
                    windowUI.Tab_Proj1.Background = black;
                    windowUI.Tab_Proj2.Background = noColor;
                    windowUI.Tab_Proj3.Background = noColor;
                    windowUI.labeltab_proj1.Foreground = white;
                    windowUI.labeltab_proj2.Foreground = black;
                    windowUI.labeltab_proj3.Foreground = black;
                    break;
                case "Project2":
                    windowUI.Tab_Proj2.Visibility = System.Windows.Visibility.Visible;
                    windowUI.Tab_Proj1.Background = noColor;
                    windowUI.Tab_Proj2.Background = black;
                    windowUI.Tab_Proj3.Background = noColor;
                    windowUI.labeltab_proj1.Foreground = black;
                    windowUI.labeltab_proj2.Foreground = white;
                    windowUI.labeltab_proj3.Foreground = black;
                    break;
                case "Project3":
                    windowUI.Tab_Proj3.Visibility = System.Windows.Visibility.Visible;
                    windowUI.Tab_Proj1.Background = noColor;
                    windowUI.Tab_Proj2.Background = noColor;
                    windowUI.Tab_Proj3.Background = black;
                    windowUI.labeltab_proj1.Foreground = black;
                    windowUI.labeltab_proj2.Foreground = black;
                    windowUI.labeltab_proj3.Foreground = white;
                    break;
                case "Home":
                    windowUI.Tab_Proj1.Background = noColor;
                    windowUI.Tab_Proj2.Background = noColor;
                    windowUI.Tab_Proj3.Background = noColor;
                    windowUI.labeltab_proj1.Foreground = black;
                    windowUI.labeltab_proj2.Foreground = black;
                    windowUI.labeltab_proj3.Foreground = black;
                    break;
            }
        }
        public static void Unload(string project)
        {
            Project1.Model.Public.Unload unloadProj1 = new Project1.Model.Public.Unload();
            Project2.Model.Public.Unload unloadProj2 = new Project2.Model.Public.Unload();
            Project3.Model.Public.Unload unloadProj3 = new Project3.Model.Public.Unload();

            foreach (UserControl item in windowUI.Content.Children)
            {
                if (item.Name == project)
                {
                    windowUI.Content.Children.Remove(item);
                    if (project == "Project1") unloadProj1.unload();
                    if (project == "Project2") unloadProj2.unload();
                    if (project == "Project3") unloadProj3.unload();
                    Open("Home");
                    break;
                }
            }

        }


    }
}
