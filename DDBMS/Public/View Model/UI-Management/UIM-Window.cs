﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Public.View_Model.UI_Management
{
    public static class UIM_Window
    {
        public static System.Windows.Window mainWindow { get; set; }
        public static void Minimize()
        {
            mainWindow.WindowState = System.Windows.WindowState.Minimized;
        }
        public static void Maxitore()
        {
            if (mainWindow.WindowState == System.Windows.WindowState.Maximized)
                mainWindow.WindowState = System.Windows.WindowState.Normal;
            else
                mainWindow.WindowState = System.Windows.WindowState.Maximized;
        }
        public static void Normal()
        {
            mainWindow.WindowState = System.Windows.WindowState.Normal;
        }
    }
}
