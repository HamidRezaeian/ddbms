﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Public.View.Sub_View.Home
{
    /// <summary>
    /// Interaction logic for HomeUI.xaml
    /// </summary>
    public partial class HomeUI : UserControl
    {
        public HomeUI()
        {
            InitializeComponent();
            ButtonProject1.Click += ButtonProject1_Click;
            ButtonProject2.Click += ButtonProject2_Click;
            ButtonProject3.Click += ButtonProject3_Click;
        }

        private void ButtonProject1_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.Open("Project1");
        }
        private void ButtonProject2_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.Open("Project2");
        }
        private void ButtonProject3_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.Open("Project3");
        }


    }
}
