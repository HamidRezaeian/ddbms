﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Public.View.Sub_View.WindowUI
{
    /// <summary>
    /// Interaction logic for WindowUI.xaml
    /// </summary>
    public partial class WindowUI : UserControl
    {
        public WindowUI()
        {
            InitializeComponent();
            Public.View_Model.UI_Management.UIM_WindowUI.windowUI = this;
            Border_Header.MouseLeftButtonDown += Border_Header_MouseLeftButtonDown;
            Border_TopHeader.MouseLeftButtonDown += Border_Header_MouseLeftButtonDown;
            ButtonMaxitore.Click += ButtonMaxitore_Click;
            ButtonMinimize.Click += ButtonMinimize_Click;
            ButtonClose.Click += ButtonClose_Click;
            ButtonHome.Click += ButtonHome_Click;
            Tab_Proj1.MouseLeftButtonDown += Tab_Proj1_MouseLeftButtonDown;
            Tab_Proj2.MouseLeftButtonDown += Tab_Proj2_MouseLeftButtonDown;
            Tab_Proj3.MouseLeftButtonDown += Tab_Proj3_MouseLeftButtonDown;
            Tab_Proj1.IsVisibleChanged += Tab_Proj1_IsVisibleChanged;
            Tab_Proj2.IsVisibleChanged += Tab_Proj2_IsVisibleChanged;
            Tab_Proj3.IsVisibleChanged += Tab_Proj3_IsVisibleChanged;
            ButtonCloseProj1.Click += ButtonCloseProj1_Click;
            ButtonCloseProj2.Click += ButtonCloseProj2_Click;
            ButtonCloseProj3.Click += ButtonCloseProj3_Click;

        }

        private void Tab_Proj1_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false) Public.View_Model.UI_Management.UIM_WindowUI.Unload("Project1");
        }

        private void ButtonCloseProj1_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.windowUI.Tab_Proj1.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseProj3_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.windowUI.Tab_Proj3.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseProj2_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.windowUI.Tab_Proj2.Visibility = Visibility.Collapsed;
        }
        private void Tab_Proj3_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false) Public.View_Model.UI_Management.UIM_WindowUI.Unload("Project3");
        }
        private void Tab_Proj2_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)Public.View_Model.UI_Management.UIM_WindowUI.Unload("Project2");
        }

        private void Tab_Proj3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.Open("Project3");
        }

        private void Tab_Proj2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.Open("Project2");
        }

        private void Tab_Proj1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.Open("Project1");
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_Window.mainWindow.Close();
        }

        private void ButtonMinimize_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_Window.Minimize();
        }

        private void ButtonMaxitore_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_Window.Maxitore();
            if (Public.View_Model.UI_Management.UIM_Window.mainWindow.WindowState == WindowState.Normal)
                Public.View_Model.UI_Management.UIM_WindowUI.windowUI.Border_TopHeader.Visibility = Visibility.Visible;
            
            else
                Public.View_Model.UI_Management.UIM_WindowUI.windowUI.Border_TopHeader.Visibility = Visibility.Collapsed;
        }

        private void ButtonHome_Click(object sender, RoutedEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_WindowUI.Open("Home");
        }


        private void Border_Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Public.View_Model.UI_Management.UIM_Window.mainWindow.DragMove();
        }
    }
}
