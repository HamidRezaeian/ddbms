﻿#pragma checksum "..\..\..\..\..\Project2\View\Main-View\Main.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "33B88EBC6345229C3ACABD4568E89AD8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AurelienRibon.Ui.SyntaxHighlightBox;
using DDBMS.Project2.Model.Transaction;
using DDBMS.Project2.View.Main_View;
using DDBMS.Project2.View.Sub_View.Console;
using DDBMS.Project2.View.Sub_View.add_transaction;
using DDBMS.Project2.View.Sub_View.lock_display;
using DDBMS.Project2.View.Sub_View.lock_management;
using Microsoft.Expression.Interactivity.Core;
using Microsoft.Expression.Interactivity.Input;
using Microsoft.Expression.Interactivity.Layout;
using Microsoft.Expression.Interactivity.Media;
using Microsoft.Expression.Media.Effects;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DDBMS.Project2.View.Main_View {
    
    
    /// <summary>
    /// Main
    /// </summary>
    public partial class Main : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DDBMS.Project2.View.Main_View.Main Project2;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnChecked1_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnUnchecked1_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnChecked2_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnUnchecked2_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnLoaded1_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border border;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem menuItem;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkBox;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkBox1;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton RadioBox_GenerateBig;
        
        #line default
        #line hidden
        
        
        #line 173 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton RadioBox_GenerateNormal;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton RadioBox_GenerateSmall;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridSplitter gridSplitter;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DDBMS.Project2.View.Sub_View.lock_management.LockBaseManagementUI lockBaseManagementUI;
        
        #line default
        #line hidden
        
        
        #line 196 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DDBMS.Project2.View.Sub_View.add_transaction.ADDT aDDT;
        
        #line default
        #line hidden
        
        
        #line 206 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DDBMS.Project2.View.Sub_View.Console.Console console;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DDBMS;component/project2/view/main-view/main.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Project2\View\Main-View\Main.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Project2 = ((DDBMS.Project2.View.Main_View.Main)(target));
            return;
            case 2:
            this.OnChecked1_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 3:
            this.OnUnchecked1_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 4:
            this.OnChecked2_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 5:
            this.OnUnchecked2_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 6:
            this.OnLoaded1_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 7:
            this.border = ((System.Windows.Controls.Border)(target));
            return;
            case 8:
            this.menuItem = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 9:
            this.checkBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 10:
            this.checkBox1 = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.RadioBox_GenerateBig = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 12:
            this.RadioBox_GenerateNormal = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 13:
            this.RadioBox_GenerateSmall = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 14:
            this.gridSplitter = ((System.Windows.Controls.GridSplitter)(target));
            return;
            case 15:
            this.grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 16:
            this.lockBaseManagementUI = ((DDBMS.Project2.View.Sub_View.lock_management.LockBaseManagementUI)(target));
            return;
            case 17:
            this.aDDT = ((DDBMS.Project2.View.Sub_View.add_transaction.ADDT)(target));
            return;
            case 18:
            this.console = ((DDBMS.Project2.View.Sub_View.Console.Console)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

