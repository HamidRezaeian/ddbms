﻿#pragma checksum "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E01BDBE07009896239FBC8AA59DE25C4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DDBMS.Project3.View.Sub_View.Hash_Join;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DDBMS.Project3.View.Sub_View.Hash_Join {
    
    
    /// <summary>
    /// HashJoinUI
    /// </summary>
    public partial class HashJoinUI : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGrid;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Duration_Copy;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Duration;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_StringConfig_Copy;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_StringConfig;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_NumericConfig_Copy;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_NumericConfig;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_key_Copy;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_key;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_type_Copy;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_type;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Duplicate_Copy;
        
        #line default
        #line hidden
        
        
        #line 188 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Duplicate;
        
        #line default
        #line hidden
        
        
        #line 198 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Duplicate_Copy1;
        
        #line default
        #line hidden
        
        
        #line 206 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Conditions;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DDBMS;component/project3/view/sub-view/hash_join/hashjoinui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Join\HashJoinUI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 2:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.label_Duration_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.label_Duration = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.label_StringConfig_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.label_StringConfig = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.label_NumericConfig_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.label_NumericConfig = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.label_key_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.label_key = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.label_type_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.label_type = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.label_Duplicate_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.label_Duplicate = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.label_Duplicate_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.label_Conditions = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

