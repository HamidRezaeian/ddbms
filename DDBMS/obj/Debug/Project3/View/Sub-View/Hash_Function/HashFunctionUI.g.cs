﻿#pragma checksum "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E2DF159004C5A45728FDEEFD6858D262"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DDBMS.Project3.View.Sub_View.Hash_Function;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DDBMS.Project3.View.Sub_View.Hash_Function {
    
    
    /// <summary>
    /// HashFunctionUI
    /// </summary>
    public partial class HashFunctionUI : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 80 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnChecked1string_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnChecked3string_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnChecked1numeric_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnChecked2numeric_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnChecked4tring_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Animation.BeginStoryboard OnChecked2string_BeginStoryboard;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButton1numeric;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButton2numeric;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1_Copy;
        
        #line default
        #line hidden
        
        
        #line 192 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButton1string;
        
        #line default
        #line hidden
        
        
        #line 193 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButton2string;
        
        #line default
        #line hidden
        
        
        #line 194 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButton3string;
        
        #line default
        #line hidden
        
        
        #line 195 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButton4string;
        
        #line default
        #line hidden
        
        
        #line 205 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 216 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 226 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        
        #line 234 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label_Key;
        
        #line default
        #line hidden
        
        
        #line 244 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy1;
        
        #line default
        #line hidden
        
        
        #line 252 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label_KeyType;
        
        #line default
        #line hidden
        
        
        #line 264 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border BorderError;
        
        #line default
        #line hidden
        
        
        #line 278 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label_Error;
        
        #line default
        #line hidden
        
        
        #line 286 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy2;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DDBMS;component/project3/view/sub-view/hash_function/hashfunctionui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\Project3\View\Sub-View\Hash_Function\HashFunctionUI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OnChecked1string_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 2:
            this.OnChecked3string_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 3:
            this.OnChecked1numeric_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 4:
            this.OnChecked2numeric_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 5:
            this.OnChecked4tring_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 6:
            this.OnChecked2string_BeginStoryboard = ((System.Windows.Media.Animation.BeginStoryboard)(target));
            return;
            case 7:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.radioButton1numeric = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 9:
            this.radioButton2numeric = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 10:
            this.label3 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.label1_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.radioButton1string = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 13:
            this.radioButton2string = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 14:
            this.radioButton3string = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 15:
            this.radioButton4string = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 16:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.Label_Key = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.label_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.Label_KeyType = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.BorderError = ((System.Windows.Controls.Border)(target));
            return;
            case 23:
            this.Label_Error = ((System.Windows.Controls.Label)(target));
            return;
            case 24:
            this.label_Copy2 = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

