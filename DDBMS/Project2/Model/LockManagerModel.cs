﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model
{
    public class LockManagerModel
    {
        public string TName { get; set; }
        private List<string> lock_x = new List<string>();
        private List<string> lock_s = new List<string>();
        private List<string> wait = new List<string>();

        public List<string> Lock_x
        {
            get { return lock_x; }
            set { lock_x = value; }
        }
        public List<string> Lock_s
        {
            get { return lock_s; }
            set { lock_s = value; }
        }
        public List<string> Wait
        {
            get { return wait; }
            set { wait = value; }
        }
    }
}
