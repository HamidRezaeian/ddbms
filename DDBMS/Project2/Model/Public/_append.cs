﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model.Public
{
    class _append
    {
        public string append(string[] s,int start=0,int remove_len=1)
        {
            if (s.Length > 0)
            {
                StringBuilder temp = new StringBuilder();
                for (int i = start; i < s.Length; i++)
                {
                    temp.Append(s[i]);
                    temp.Append(',');
                }
                temp = temp.Remove(temp.Length - remove_len, remove_len);
                return temp.ToString();
            }
            else return null;
        }
    }
}
