﻿using DDBMS.Project2.View_Model.UI_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model.Public
{
    class Unload
    {
        public void unload()
        {
            UIM_Autosim.autosim.dispatcherTimer.Stop();
            #region Unload_LockManager
            Project2.Model.LockManager.LockBase_List.Clear();
            Project2.Model.LockManager.Lock_commit = false;
            Project2.Model.LockManager.Transaction_List.Clear();
            Project2.Model.LockManager.Wait_List.Clear();
            #endregion
            #region Unload_Tproducer
            Project2.Model.Transaction.Tproducer.Len_RandomString = "ABCDEF";
            Project2.Model.Transaction.Tproducer.Len_RandomInt = "12345";
            Project2.Model.Transaction.Tproducer.TStart = 0;
            #endregion

        }
    }
}
