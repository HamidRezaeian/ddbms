﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model.Public
{
    class Randomize
    {
        private Random random = new Random();
        public string Create(string input,int length = 1)
        {
            string chars = input;
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
