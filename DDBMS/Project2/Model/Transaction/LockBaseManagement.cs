﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model.Transaction
{
    class LockBaseManagement
    {
        public string Create(string[] T,string LockOffer)
        {
            bool has_slock = new bool();
            var item2 = LockManager.LockBase_List.FirstOrDefault(i => i.TName == T[0]);
            bool Duplicates = (item2 != null) ? true : false;
            foreach (var item in LockManager.LockBase_List)
            {
                if (item.TName != T[0])
                {
                    
                    for (int i = 0; i < item.Lock_x.Count(); i++)
                    {
                        
                        if (item.Lock_x[i] == T[1])
                        {
                            if (Duplicates == true)
                                item2.Wait.Add(T[1]);
                            else
                                LockManager.LockBase_List.Add(new LockManagerModel { TName = T[0], Wait =new List<string> { T[1] } });
                            Project2.View_Model.UI_Management.UIM_Console.Set(string.Format("\t Not Grant Lock_{0}({2},{1}).", LockOffer, T[0], T[1]));
                            Model.LockManager.Wait_List.Add(new string[] {T[0],T[1],LockOffer});
                            return null;
                        }
                    }
                    for (int i = 0; i < item.Lock_s.Count(); i++) if (item.Lock_s[i] == T[1]) has_slock = true;
                }
            }
                if (Duplicates == true)
                {

                    switch (LockOffer)
                    {
                        case "X":
                        if (has_slock)
                        {
                            item2.Wait.Add(T[1]);
                            Model.LockManager.Wait_List.Add(new string[] { T[0], T[1], LockOffer });
                            Project2.View_Model.UI_Management.UIM_Console.Set(string.Format("\t Not Grant Lock_{0}({2},{1}).", LockOffer, T[0], T[1]));
                            return null;
                        }
                            item2.Lock_x.Add(T[1]);
                            break;
                        case "S":
                            item2.Lock_s.Add(T[1]);
                            break;
                        default:
                            break;
                    }
                    
                }
                else
                {
                     switch (LockOffer)
                       {
                          case "X":
                        if (has_slock)
                        {
                            LockManager.LockBase_List.Add(new LockManagerModel { TName = T[0], Wait = new List<string> { T[1] } });
                            Model.LockManager.Wait_List.Add(new string[] { T[0], T[1], LockOffer });
                            Project2.View_Model.UI_Management.UIM_Console.Set(string.Format("\t Not Grant Lock_{0}({2},{1}).", LockOffer, T[0], T[1]));
                            return null;
                        }
                             LockManager.LockBase_List.Add(new LockManagerModel { TName = T[0], Lock_x = new List<string> { T[1] } });
                             break;
                         case "S":
                             LockManager.LockBase_List.Add(new LockManagerModel { TName = T[0], Lock_s = new List<string> { T[1] } });
                             break;
                            default:
                               break;
                      }
                }
            Project2.View_Model.UI_Management.UIM_Console.Set(string.Format("\t Grant Lock_{0}({2},{1}).", LockOffer, T[0], T[1]));

            return LockOffer;
        }

        public void Update()
        {
            if (Model.LockManager.LockBase_List.Count>0)
            View_Model.UI_Management.UIM_Console.Set(string.Format("\t Update..."));

            string LockOffer = string.Empty;
            foreach (var item in Model.LockManager.LockBase_List)
            {
                List<string> temp = new List<string>();
                //var item2 = LockManager.LockBase_List.FirstOrDefault(i => i.TName == item.TName);
                if (item.Wait!=null)
                {
                    foreach (var item1 in item.Wait)
                    {
                        temp.Add(item1);
                    }
                }
                foreach (var item3 in temp)
                {
                    string[] tempwait = new string[3];
                    foreach (var item4 in Model.LockManager.Wait_List)
                    {
                        if (item.TName == item4[0] && item3 == item4[1])
                        {
                            LockOffer = item4[2];
                            tempwait = item4;
                            break;
                        }
                    }

                    Model.LockManager.Wait_List.Remove(tempwait);
                    //item2.Wait.Remove(item3);
                    item.Wait.Remove(item3);
                    string ss = Create(new string[] { item.TName, item3 },LockOffer);
                    DDBMS.Project2.View_Model.UI_Management.UIM_LockDisplay.Edit_Item(item3, item.TName, item.TName, ss );

                }
            }
        }
    }
}
