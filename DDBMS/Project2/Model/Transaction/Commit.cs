﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model.Transaction
{
    class Commit
    {
        public bool IsCommit(string Tname,out List<string> s)
        {
            
            List<string> temp = new List<string>();
            foreach (var item in LockManager.LockBase_List)
            {
                if (item.TName == Tname)
                    if (item.Wait.Count == 0)
                    {
                        s = null;
                        return true;
                    }
                    else
                    {
                        temp = item.Wait;
                    }
            }
            s = temp;
            return false;


        }
    }
}
