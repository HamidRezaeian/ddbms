﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model.Transaction
{
    class Abort
    {
        public void rollback(string Tname)
        {
            List<string> Tnames = new List<string>();
            Tnames.Add(Tname);
            do
            {
                ObservableCollection<LockManagerModel> Txlock = GiveXLocks(Tnames);
                foreach (var item in Tnames)
                {
                     
                    foreach (var item1 in LockManager.LockBase_List)
                    {
                        if (item1.TName==item)
                            if ( item1.Wait.Count>0 && item1.Lock_x.Count==0 && item1.Lock_s.Count==0) View_Model.UI_Management.UIM_Console.Set(string.Format("\t Transaction {0} Release.", item));
                            else View_Model.UI_Management.UIM_Console.Set(string.Format("\t Transaction {0} Roll-Back.", item));
                    }
                    LockManager.RemoveFrom_TLists(item);
                }
                Tnames = FindDuplicatesV(Txlock);
            } while (Tnames.Count!=0);

        }
        private ObservableCollection<LockManagerModel> GiveXLocks(List<string> Tnames)
        {
            ObservableCollection<LockManagerModel> temp = new ObservableCollection<LockManagerModel>();
            foreach (var item1 in Tnames)
            {
                foreach (var item in LockManager.LockBase_List)
                {
                    if (item.TName == item1) temp.Add(new LockManagerModel() { TName=item.TName,Lock_x= item.Lock_x });
                }
            }

            return temp;
        }
        private List<string> FindDuplicatesV(ObservableCollection<LockManagerModel> lockx)
        {
            if (lockx!=null)
            {
                List<string> templist = new List<string>();
                foreach (var item1 in lockx)
                {
                    foreach (var item in LockManager.Transaction_List)
                    {
                        foreach (string s in item)
                        {
                            foreach (var item2 in item1.Lock_x)
                            {
                                if (s == item2 &&
                                    Convert.ToInt32(item1.TName.Substring(1,item1.TName.Length-1)) < Convert.ToInt32(item[0].Substring(1,item[0].Length-1)))
                                    templist.Add(item[0]);
                            }

                        }
                    }
                }
                return templist;
            }
            else return null;
        }
    }
}
