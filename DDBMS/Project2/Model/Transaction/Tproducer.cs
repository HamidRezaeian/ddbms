﻿using DDBMS.Project2.Model.Public;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model.Transaction
{
    public static class Tproducer
    {
        //private static Random random = new Random();
        private static string[] Transaction;
        public  static int TStart = 0;
        public static string Len_RandomString = "ABCDEF";//ABCDEFGHIJKLMNOPQRSUVWXYZ
        public static string Len_RandomInt = "12345";//123456789

        public static string[] Create()
        {
            Randomize random = new Randomize();
            Transaction = new string[10];
            Transaction[0] = string.Format("T{0}", TStart++);
            for (int i = 1; i < Convert.ToInt16(random.Create(Len_RandomInt))+1; i++)
            {
                Transaction[i] = string.Format("{0}{1}", random.Create(Len_RandomString), random.Create(Len_RandomInt));
            }
            Transaction = Transaction.Distinct().ToArray();
            Transaction = Transaction.Where(c => c != null).ToArray();
            //LockManager.Transaction_List.Add(Transaction);
            return Transaction;
        }

    }
}
