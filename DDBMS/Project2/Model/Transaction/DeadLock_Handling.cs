﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.Model.Transaction
{
    class DeadLock_Handling
    {
        int[] _abortList = new int[Model.LockManager.LockBase_List.Count];
        public void HandleIt()
        {
            List<string> temp = new List<string>();
            View_Model.UI_Management.UIM_Console.Set(string.Format("--Oops DeadLock Happened! \n\t DeadLock Handling Processing..."));
            foreach (var item in Model.LockManager.LockBase_List)
            {
                Cost(item.TName);
            }
            int _bestCost = _abortList[0];
            int _index = 0;
            for (int i = 1; i < _abortList.Count(); i++)
            {
                if (_abortList[i] >= _bestCost)
                {
                    _bestCost = _abortList[i];
                    _index = i;
                }
            }
            Model.Public._append append = new Public._append();
            View_Model.UI_Management.UIM_Console.Set(string.Format("\t Transaction {0} Choosen By DeadLock Handler To Abort .(Transaction Cost = {1}[{2}] <= ({3}))", Model.LockManager.LockBase_List[_index].TName, _bestCost,_index, append.append( _abortList.Select(i => i.ToString()).ToArray())));

            Model.LockManager.Abort(Model.LockManager.LockBase_List[_index].TName);
        }
        private List<string> Cost(string Tname)
        {
            int _cost = 0;
            List<string> temp = new List<string>();
            
            foreach (var item in Model.LockManager.LockBase_List)
            {
                if (item.TName==Tname)
                {
                    bool[] _change = new bool[_abortList.Count()];
                    foreach (var item1 in item.Wait)
                    {
                        
                        for (int i=0;i< Model.LockManager.LockBase_List.Count;i++)
                        {
                            if (Model.LockManager.LockBase_List[i].TName != Tname)
                            {
                                foreach (var item3 in Model.LockManager.LockBase_List[i].Lock_x)
                                {
                                    if (item3 == item1)
                                    {
                                        _cost++;
                                        temp.Add(Model.LockManager.LockBase_List[i].TName);
                                        if (!_change[i])
                                        {
                                            //_abortList[i]++;
                                            _abortList[i] = 0;
                                            _change[i] = true;
                                        }

                                    }
                                }
                                foreach (var item3 in Model.LockManager.LockBase_List[i].Lock_s)
                                {
                                    if (item3 == item1)
                                    {
                                        _cost++;
                                        temp.Add(Model.LockManager.LockBase_List[i].TName);
                                        if (!_change[i])
                                        {
                                            _abortList[i]++;
                                            _change[i] = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            }
            return temp;
            //abortlist = temp;
            //return _cost;
        }
    }
}
