﻿using DDBMS.Project2.Model.Transaction;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace DDBMS.Project2.Model
{
    public static class LockManager
    {
        public static bool Lock_commit = false;
        public static ObservableCollection<string[]> Transaction_List = new ObservableCollection<string[]>();
        public static ObservableCollection<string[]> Wait_List = new ObservableCollection<string[]>();

        private static ObservableCollection<LockManagerModel> lockBase_List = new ObservableCollection<LockManagerModel>();
        public static ObservableCollection<LockManagerModel> LockBase_List
        {
            get
            {
                return lockBase_List;
            }
            set
            {
                lockBase_List = value;
            }
        }

        public static void Add_Transaction(string[] Transaction=null)
        {
            Texecute tx = new Texecute();

            if (Transaction != null)
            {
                tx.execute(Transaction);
            }
            else
            {
                tx.execute(Tproducer.Create());
            }
        }
        public static void Commit(string Tname=null)
        {
            Lock_commit = true;
            Model.Transaction.DeadLock_Handling _deadlock = new DeadLock_Handling();
            Model.Transaction.Commit _commit = new Transaction.Commit();
            List<string> temp = new List<string>();
            Model.Public._append append = new Public._append();
            
            if (LockBase_List.Count > 0 && Transaction_List.Count>0)
            {
                Random random = new Random();
                if (Tname == null)
                {
                    foreach (var item in LockBase_List)
                    {
                        if (item.Wait.Count == 0)
                        {
                            Tname = item.TName;
                            break;
                        }
                    }
                }
                if (Tname == null)
                {
                    _deadlock.HandleIt();
                    Commit();
                }
                    if (Tname != null)
                    View_Model.UI_Management.UIM_Console.Set(string.Format("Transaction {0} On Commit Process...", Tname));

                if (_commit.IsCommit(Tname,out temp))
                {
                    View_Model.UI_Management.UIM_Console.Set(string.Format("Transaction {0} Commit.[{0} Release]", Tname));
                    Model.Transaction.LockBaseManagement lbmanage = new Model.Transaction.LockBaseManagement();
                    RemoveFrom_TLists(Tname);
                    lbmanage.Update();
                }
                else
                {
                    if (Tname!=null)
                    View_Model.UI_Management.UIM_Console.Set(string.Format("\t Transaction {0} Can't Commit => Variable {1} Is On The {0} Waiting List.", Tname,append.append(temp.ToArray())));

                }
            }
            Lock_commit = false;
        }
        public static void Abort(string Tname)
        {
            Model.Transaction.Abort abort = new Transaction.Abort();
            Model.Transaction.LockBaseManagement lbmanage = new Model.Transaction.LockBaseManagement();

            View_Model.UI_Management.UIM_Console.Set(string.Format("Transaction {0} Abort.", Tname));
            abort.rollback(Tname);
            lbmanage.Update();
        }
        private static string[] Return_Transaction(string Tname)
        {
            foreach (var item in Transaction_List)
            {
                if (item[0] == Tname)
                {
                    return item;
                }
            }
            return null;
        }
        public static void RemoveFrom_TLists(string Tname)
        {
          ObservableCollection<string[]> temp = new ObservableCollection<string[]>();
            ObservableCollection<LockManagerModel> temp1 = new ObservableCollection<LockManagerModel>();
            foreach (var item in Transaction_List)
                {
                    if (item[0] == Tname)
                    {
                    temp.Add(item);
                        for (int i = 1; i < item.Length; i++)
                        {
                            DDBMS.Project2.View_Model.UI_Management.UIM_LockDisplay.Edit_Item(item[i], item[0], null, null);
                        }

                    }
                }
            foreach (var item in LockBase_List)
            {
                if (item.TName == Tname)
                {
                    temp1.Add(item);
                }
            }
            foreach (var item in temp)
            {
                Transaction_List.Remove(item);
            }
            foreach (var item in temp1)
            {
                LockBase_List.Remove(item);
            }
        }
           
            
        
    }
}
