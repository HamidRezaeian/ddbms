﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDBMS.Project2.View.Sub_View.lock_display;
using System.Windows.Controls;
using System.Windows;
using System.Data;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.ComponentModel;

namespace DDBMS.Project2.View_Model.UI_Management
{
    public static class UIM_LockDisplay
    {
        public static DDBMS.Project2.View.Sub_View.lock_display.LockDisplayUI lockdisplay { get; set; }
        #region AddItem
        private static Brush Random_color()
        {
            Random r = new Random();
            Brush brush = new SolidColorBrush(Color.FromRgb((byte)r.Next(1, 200),
              (byte)r.Next(1, 150), (byte)r.Next(1, 150)));
            return brush;
        }
        public static void Add_Item(string variable,string T,string LockBase)
        {

                if (addT(variable, T, LockBase)==false)
                { 
                    DDBMS.Project2.View.Sub_View.lock_display.ObjectModel om = new ObjectModel();
                    Brush color = Random_color();
                    om.Height = 52;
                    om.BorderVariable.Background = color;
                    om.label_Variable.Content = variable;
                    lockdisplay.StackPanel1.Children.Add(om);
                    addT(variable, T, LockBase);
                 }
        }
        public static bool addT(string variable, string T,string LockBase)
        {
            foreach (DDBMS.Project2.View.Sub_View.lock_display.ObjectModel item in lockdisplay.StackPanel1.Children)
            {
                if (item.label_Variable.Content as string == variable)
                {
                    DDBMS.Project2.View.Sub_View.lock_display.SubObjectModel o = new SubObjectModel();
                    Edit_Item(variable, T, null, null,false);
                    o.Transaction.Content = T;
                    o.Transaction.Foreground = item.BorderVariable.Background;
                    o.LockBase.Content = LockBase;
                    o.LockBaseBorder.Visibility = Visibility.Visible;
                    if (LockBase == null) o.LockBaseBorder.Visibility = Visibility.Collapsed;
                    item.stackPanelT.Children.Add(o);
                    return true;
                }
            }
            return false;
        }
        #endregion
        #region EditItem
        public static void Edit_Item(string variable,string T_OldValue,string T_NewValue,string NewLockbase,bool CheckForClean=true)
        {
            foreach (DDBMS.Project2.View.Sub_View.lock_display.ObjectModel item in lockdisplay.StackPanel1.Children)
            {
                if (item.label_Variable.Content as string == variable)
                {
                    ObservableCollection<SubObjectModel> temp = new ObservableCollection<SubObjectModel>();
                    foreach (DDBMS.Project2.View.Sub_View.lock_display.SubObjectModel item1 in item.stackPanelT.Children)
                    {
                        if (item1.Transaction.Content as string == T_OldValue)
                        {

                            if (T_NewValue == null && NewLockbase == null)
                            {
                                temp.Add(item1);
                            }
                            else
                            {

                                item1.Transaction.Content = (T_NewValue != null) ? T_NewValue : item1.Transaction.Content;
                                if (NewLockbase != null)
                                {
                                    item1.LockBase.Content = NewLockbase;
                                    item1.LockBaseBorder.Visibility = Visibility.Visible;
                                    item1.LockBaseStoryBoard_BeginStoryboard.Storyboard.Begin();
                                }
                            }

                        }

                    }
                    if (temp != null)
                    {
                        foreach (var item2 in temp)
                        {
                            item2.SubObjectModelUnload_StoryBoard_BeginStoryboard.Storyboard.Completed += (Null, Nullable) =>
                            {
                                item.stackPanelT.Children.Remove(item2);
                                if (CheckForClean)
                                {
                                    if (item.stackPanelT.Children.Count == 0)
                                    {
                                        item.LockManagerObjectModelUnload_StoryBoard_BeginStoryboard.Storyboard.Completed += (Null1, Nullable1) =>
                                        {
                                            lockdisplay.StackPanel1.Children.Remove(item);
                                        };
                                        item.LockManagerObjectModelUnload_StoryBoard_BeginStoryboard.Storyboard.Begin();
                                    }
                                }
                                
                            };
                            item2.SubObjectModelUnload_StoryBoard_BeginStoryboard.Storyboard.Begin();
                        }
                    }
                }
            }
        }



        #endregion
    }
}
