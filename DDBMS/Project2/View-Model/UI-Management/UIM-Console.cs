﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDBMS.Project2.View_Model.UI_Management
{
    public static class UIM_Console
    {
        public static View.Sub_View.Console.Console console { get; set; }
        public static void Set(string s)
        {
            if (console.checkBox.IsChecked.Value && console.Visibility== System.Windows.Visibility.Visible)
            console.shbox.Text = console.shbox.Text+ string.Format("{0}\n",s);
        }
    }
}
