﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AurelienRibon;
using AurelienRibon.Ui.SyntaxHighlightBox;

namespace DDBMS.Project2.View.Main_View
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : UserControl
    {
        public Main()
        {
            InitializeComponent();
            RadioBox_GenerateBig.Checked += RadioBox_GenerateBig_Checked;
            RadioBox_GenerateNormal.Checked += RadioBox_GenerateNormal_Checked;
            RadioBox_GenerateSmall.Checked += RadioBox_GenerateSmall_Checked;
        }

        private void RadioBox_GenerateSmall_Checked(object sender, RoutedEventArgs e)
        {
            Model.Transaction.Tproducer.Len_RandomString = "ABC";
            Model.Transaction.Tproducer.Len_RandomInt = "123";
        }

        private void RadioBox_GenerateNormal_Checked(object sender, RoutedEventArgs e)
        {
            Model.Transaction.Tproducer.Len_RandomString = "ABCDEF";
            Model.Transaction.Tproducer.Len_RandomInt = "12345";
        }

        private void RadioBox_GenerateBig_Checked(object sender, RoutedEventArgs e)
        {
            Model.Transaction.Tproducer.Len_RandomString = "ABCDEFGHIJKLMNOPQRSUVWXYZ";
            Model.Transaction.Tproducer.Len_RandomInt = "123456789";
        }
    }
}
