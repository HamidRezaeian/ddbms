﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDBMS.Project2.View.Sub_View.lock_management
{
    /// <summary>
    /// Interaction logic for ObjectModel.xaml
    /// </summary>
    public partial class ObjectModel : UserControl
    {
        public ObjectModel()
        {
            InitializeComponent();
            Button_commit.MouseLeftButtonUp += Button_commit_MouseLeftButtonUp;
            Button_abort.MouseLeftButtonUp += Button_abort_MouseLeftButtonUp;
        }

        private void Button_abort_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Model.LockManager.Abort(textBlock.Text);
        }

        private void Button_commit_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Model.LockManager.Commit(textBlock.Text);
        }


    }
}
