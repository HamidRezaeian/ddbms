﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DDBMS.Project2.View.Sub_View.lock_management.AutoSim
{
    /// <summary>
    /// Interaction logic for AutosimUI.xaml
    /// </summary>
    public partial class AutosimUI : UserControl
    {
        public DispatcherTimer dispatcherTimer = new DispatcherTimer();
        int _slowerCount = 250;
        int _FasterCount = 250;
        int _DefaultTime = 4000;
        int _CureentTime = 0;
        public AutosimUI()
        {
            InitializeComponent();
            Project2.View_Model.UI_Management.UIM_Autosim.autosim = this;
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            labelStart.MouseLeftButtonUp += LabelStart_MouseLeftButtonUp;
            labelStop.MouseLeftButtonUp += LabelStop_MouseLeftButtonUp;
            labelTimeDefault.MouseLeftButtonUp += LabelTimeDefault_MouseLeftButtonUp;
            labelTimeSlow.MouseLeftButtonUp += LabelTimeSlow_MouseLeftButtonUp;
            labelTimeFast.MouseLeftButtonUp += LabelTimeFast_MouseLeftButtonUp;
            _CureentTime = _DefaultTime;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, _CureentTime);
           
            // SetLabelSpeed();
        }

        private void LabelTimeFast_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_CureentTime > _FasterCount)
            {
                _CureentTime -= _FasterCount;
                dispatcherTimer.Interval = new TimeSpan(0, 0, 0,0, _CureentTime);
                SetLabelSpeed();
            }
        }

        private void LabelTimeSlow_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_CureentTime < 2000000000)
            {
                _CureentTime += _slowerCount;
                dispatcherTimer.Interval = new TimeSpan(0, 0, 0,0, _CureentTime);
                SetLabelSpeed();
            }
        }

        private void LabelTimeDefault_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, _DefaultTime);
            _CureentTime = _DefaultTime;
            SetLabelSpeed();
            
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (Model.LockManager.Lock_commit==false)
            Model.LockManager.Commit();
        }
        private void LabelStop_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            dispatcherTimer.Stop();
        }

        private void LabelStart_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            dispatcherTimer.Start();
        }
        private void SetLabelSpeed()
        {
            labelSpeed.Content = string.Format("{0} C/S",Math.Round( (decimal)1000/_CureentTime,2));
        }
    }
}
