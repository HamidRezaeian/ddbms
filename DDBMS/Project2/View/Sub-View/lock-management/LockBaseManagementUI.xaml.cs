﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DDBMS.Project2.View.Sub_View.lock_management
{
    /// <summary>
    /// Interaction logic for LockBaseManagementUI.xaml
    /// </summary>
    public partial class LockBaseManagementUI : UserControl
    {
        public LockBaseManagementUI()
        {
            InitializeComponent();
            ((INotifyCollectionChanged)Model.LockManager.LockBase_List).CollectionChanged += LockBaseManagementUI_CollectionChanged1;
        }





        private void LockBaseManagementUI_CollectionChanged1(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action.ToString() == "Remove")
            {
                 listBox.ItemsSource = null;
                 listBox.ItemsSource = Model.LockManager.LockBase_List;
            }
        }

    }
}
