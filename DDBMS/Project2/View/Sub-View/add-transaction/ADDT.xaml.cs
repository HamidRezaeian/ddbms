﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AurelienRibon.Ui.SyntaxHighlightBox;

namespace DDBMS.Project2.View.Sub_View.add_transaction
{
    /// <summary>
    /// Interaction logic for ADDT.xaml
    /// </summary>
    public partial class ADDT : UserControl
    {
        public ADDT()
        {
            InitializeComponent();
            shbox.CurrentHighlighter = HighlighterManager.Instance.Highlighters["VHDL"];
            Button_AddT.MouseLeftButtonDown += Button_AddT_MouseLeftButtonDown;
        }

        private void Button_AddT_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (border2.Visibility != Visibility.Visible)
            {
                if (shbox.Text != "")
                {
                    string temp = string.Format("T{0},{1}", Convert.ToString(Model.Transaction.Tproducer.TStart++), shbox.Text);
                    Model.LockManager.Add_Transaction(temp.Split(','));
                }
            }
            else
            {
                Model.LockManager.Add_Transaction();
            }
        }

    }
}
