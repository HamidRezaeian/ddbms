﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AurelienRibon.Ui.SyntaxHighlightBox;

namespace DDBMS.Project2.View.Sub_View.Console
{
    /// <summary>
    /// Interaction logic for Console.xaml
    /// </summary>
    public partial class Console : UserControl
    {
        public Console()
        {
            InitializeComponent();
            Project2.View_Model.UI_Management.UIM_Console.console = this;
            shbox.CurrentHighlighter = HighlighterManager.Instance.Highlighters["VHDL"];

            button_clear.Visibility = Visibility.Collapsed;
            shbox.TextChanged += Shbox_TextChanged;
            button_clear.Click += Button_clear_Click;
            
        }

        private void Button_clear_Click(object sender, RoutedEventArgs e)
        {
            shbox.Text = "";
        }

        private void Shbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            shbox.Focus();
            shbox.CaretIndex = shbox.Text.Length;
            button_clear.Visibility = (shbox.Text != "") ? Visibility.Visible : Visibility.Collapsed;
            
        }
    }
}
